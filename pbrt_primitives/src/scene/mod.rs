use super::bounds::Bounds3f;
use super::lights::Light;
use super::math::points::*;
use super::math::vectors::*;
use super::rays::Ray;
use super::Primitive;
use super::SurfaceIntersection;

pub struct LookAt {
    eye: Point3f,
    look_at: Point3f,
    up_vec: Vector3f,
}

impl LookAt {
    pub fn new(eye: Point3f, look_at: Point3f, up_vec: Vector3f) -> Self {
        LookAt {
            eye,
            look_at,
            up_vec,
        }
    }
}

pub struct Scene {
    lights: Vec<Light>,
    aggregate: Primitive,
    world_bound: Bounds3f,
    look_at: LookAt,
}

impl Scene {
    pub fn new(aggregate: Primitive, lights: Vec<Light>, look_at: LookAt) -> Scene {
        let world_bound = aggregate.world_bound();
        let mut scene = Scene {
            lights: Vec::new(),
            aggregate,
            world_bound,
            look_at,
        };

        let mut lights = lights;

        for l in &mut lights {
            l.preprocess(&scene);
        }

        scene.lights = lights;
        scene
    }

    pub fn world_bound(&self) -> &Bounds3f {
        &self.world_bound
    }

    pub fn intersect(&self, ray: &Ray) -> Option<SurfaceIntersection> {
        self.aggregate.intersect(ray)
    }

    pub fn intersect_any(&self, ray: &Ray) -> bool {
        self.aggregate.intersect_any(ray)
    }

    pub fn split_scene<'a>(&'a self, times: usize) -> &[Scene] {
        unimplemented!()
    }
}
