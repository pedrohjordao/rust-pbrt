use std::ops::{Index};

use math::vectors::Vector2;
use math::points::Point2;
use num::Num;

/// A struct representing the corners of a Bounding Box in 2D space
#[derive(PartialEq, Debug, Clone)]
pub struct Corners2<T: Num>([Point2<T>; 4]);

impl <T: Num> Index<usize> for Corners2<T> {
    type Output = Point2<T>;

    /// Selects one of the 4 corners of the Bounding Box
    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 4);
        &self.0[index]
    }
}

/// A structure representing a Bounding Sphere in 2D space.
/// This is structure is generic over the type of the point
/// used to represent it's center, but it's radius is always
/// a floating point .
#[derive(PartialEq, Debug, Clone)]
pub struct BoundingCircle<T: Num> {
    center: Point2<T>,
    radius: f64
}

/// A structure representing a Bounding Box in 2D space.
/// This Structure is generic over it's data type, such that
/// you can create "discrete" bounds with integer types and
/// precise bounds with floating point types.
#[derive(PartialEq, Debug, Clone)]
pub struct Bounds2<T: Num> {
    min: Point2<T>,
    max: Point2<T>,
}

pub type Bounds2f = Bounds2<f64>;
pub type Bounds2i = Bounds2<i64>;

impl<T: Num> Bounds2<T> {
    pub fn diagonal(&self) -> Vector2<T> {
        unimplemented!()
    }
}
