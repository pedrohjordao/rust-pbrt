use std::default::Default;
use std::iter::{IntoIterator, Iterator};
use std::ops::{Add, AddAssign, Div, DivAssign, Index, Mul, Sub};

use num::{Bounded, Float, Num, Signed, Integer};

use math::points::Point3;
use math::vectors::{Vector3, Vector3f};
use transform::{Transformable, Transform};
use rays::Ray;

/// A struct representing the corners of a Bounding Box in 3D space
#[derive(PartialEq, Debug, Clone)]
pub struct Corners3<T: Num>([Point3<T>; 8]);

impl<T: Num> Index<usize> for Corners3<T> {
    type Output = Point3<T>;

    /// Selects one of the 8 corners of the Bounding Box
    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 8);
        &self.0[index]
    }
}


/// A structure representing a Bounding Sphere in 3D space.
/// This is structure is generic over the type of the point
/// used to represent it's center, but it's radius is always 
/// a floating point .
#[derive(PartialEq, Debug, Clone)]
pub struct BoundingSphere<T: Num> {
    center: Point3<T>,
    radius: f64,
}

/// A structure representing a Bounding Box in 3D space.
/// This Structure is generic over it's data type, such that
/// you can create "discrete" bounds with integer types and
/// precise bounds with floating point types.
#[derive(PartialEq, Debug, Clone)]
pub struct Bounds3<T: Num> {
    min: Point3<T>,
    max: Point3<T>,
}

pub type Bounds3f = Bounds3<f64>;
pub type Bounds3i = Bounds3<i64>;



impl Transformable for Bounds3f {
    type Output = Self;

    /// Applies the transformation in every corner of the 
    /// Bounding Box, effectivly transforming the whole Bounding Box
    fn apply_transform(&self, t: &Transform) -> Self::Output {
        use std::iter::IntoIterator;
        self.corners()
            .into_iter()
            .fold(None, |acc, x| {
                match acc {
                    None => {
                        Some(Bounds3f::enclosing_point(t.apply(&x)))
                    }
                    Some(a) => {
                        use bounds::bound_point_union3;
                        Some(bound_point_union3(&a, &x))
                    }
                }
            }).unwrap()
    }
}

impl Bounds3<f64> {
    pub fn intersect_p(&self, ray: &Ray) -> Option<(f64, f64)> {
        let mut t0 = 0.;
        let mut t1 = ray.limit();

        for i in 0..3 {
            let inv_dir = 1. / ray.direction()[i];
            let mut t_near = (self.min[i] - ray.origin()[i]) * inv_dir;
            let mut t_far = (self.min[i] - ray.origin()[i]) * inv_dir;
            if t_near > t_far {
                let aux = t_near;
                t_near = t_far;
                t_far = aux;
            }
            // TODO: Update tFar to ensure robust ray–bounds intersection
            let t_near = t_near;
            let t_far = t_far;
            t0 = if t0 < t_near {
                t_near
            } else {
                t0
            };
            t1 = if t_far < t1 {
                t_far
            } else {
                t1
            };
            if t0 > t1 {
                return None;
            }
        }

        Some((t0, t1))
    }

    pub fn intersect_p_with_neg_dir(&self, ray: &Ray, inv_dir: Vector3f, dir_is_neg: [usize; 3]) -> bool {
        let mut t_min = (self[dir_is_neg[0]].x - ray.origin().x) * inv_dir.x;
        let mut t_max = (self[1 - dir_is_neg[0]].x - ray.origin().x) * inv_dir.x;
        let ty_min = (self[dir_is_neg[1]].y - ray.origin().y) * inv_dir.y;
        let ty_max = (self[1 - dir_is_neg[1]].y - ray.origin().y) * inv_dir.y;

        // TODO: Update tMax and tyMax to ensure robust bounds intersection
        if t_min > ty_max || ty_min > t_max {
            return false;
        }

        if ty_min > t_min {
            t_min = ty_min;
        }

        if ty_max < t_max {
            t_max = ty_max;
        }

        let tz_min = (self[dir_is_neg[2]].z - ray.origin().z) * inv_dir.z;
        let tz_max = (self[1 - dir_is_neg[2]].z - ray.origin().z) * inv_dir.z;
        // TODO: Update _tzMax_ to ensure robust bounds intersection

        if t_min > tz_max || tz_min > t_max {
            return false;
        }

        if tz_min > t_min {
            t_min = tz_min;
        }

        if tz_max < t_max {
            t_max = tz_max;
        }

        (t_min < ray.limit()) && (t_max > 0.)

    }

}

impl<T: Bounded + Num> Default for Bounds3<T> {
    /// Returns a Bounding Box covering the whole 3D space.
    fn default() -> Self {
        Bounds3 {
            max: Point3 {
                x: T::min_value(),
                y: T::min_value(),
                z: T::min_value(),
            },
            min: Point3 {
                x: T::max_value(),
                y: T::max_value(),
                z: T::max_value(),
            },
        }
    }
}

impl<T: Num + Clone> Bounds3<T> {
    /// Creates a bounding Bounding Box that only encloses
    /// a point in space. Useful to use with unions.
    pub fn enclosing_point(p: Point3<T>) -> Self {
        Bounds3 {
            min: p.clone(),
            max: p,
        }
    }

    /// Gets an object representing the corners of this
    /// Bounding Box. This object is able to be iterated
    /// over and has array access
    pub fn corners(&self) -> Corners3<T> {
        Corners3([
            self.corner(0),
            self.corner(1),
            self.corner(2),
            self.corner(3), 
            self.corner(4),
            self.corner(5),
            self.corner(6),
            self.corner(7)
        ])
    }

    fn corner(&self, corner: usize) -> Point3<T> {
        assert!(corner < 8);

        let x: T;
        if corner & 1 == 0 {
            x = self.min.x.clone();
        } else {
            x = self.max.x.clone();
        }

        let y: T;
        if corner & 2 == 0 {
            y = self.min.y.clone();
        } else {
            y = self.max.y.clone();
        }

        let z: T;
        if corner & 4 == 0 {
            z = self.min.z.clone();
        } else {
            z = self.max.z.clone();
        }

        Point3 { x, y, z }
    }
}

impl<T> Bounds3<T>
where
    T: Float + Add + Div<i32> + Clone + Ord + Into<f64>,
    <T as Div<i32>>::Output: Num + Clone + Into<T> + Into<f64>,
{
    /// Returns a `BoundingSphere` that encloses this Bounding Box.
    pub fn bounding_sphere(&self) -> BoundingSphere<<T as Div<i32>>::Output> {
        let center = Point3 {
            x: (self.min.x + self.max.x) / 2,
            y: (self.min.y + self.max.y) / 2,
            z: (self.min.z + self.max.z) / 2,
        };
        let cloned_self = self.clone();
        let is_inside = inside(&center.clone().casted::<T>(), &cloned_self);
        if is_inside {
            use math::points::point_distance3;
            BoundingSphere {
                center: center.clone(),
                radius: point_distance3(center.casted::<f64>(), self.max.casted::<f64>()),
            }
        } else {
            BoundingSphere {
                center,
                radius: 0.0,
            }
        }
    }
}

impl<T> Bounds3<T>
where
    T: Num + Clone + Sub,
    <T as Sub>::Output: Num,
{
    /// Returns the vector along the box diagonal from the
    /// min point to the max point
    pub fn diagonal(&self) -> Vector3<T> {
        self.min.clone() - self.max.clone()
    }

    /// Returns the volume of the Bounding Box
    pub fn volume(&self) -> <T as Mul>::Output {
        let diag = self.diagonal();
        diag.x * diag.y * diag.z
    }
}

impl<T> Bounds3<T>
where
    T: Num + Clone + Sub + Mul + Mul<i32>,
    <T as Sub>::Output: Num,
{
    /// Returns the surface area of the Bounding Box
    pub fn surface_area(&self) -> <T as Mul<i32>>::Output {
        let diag = self.diagonal();
        (diag.x.clone() * diag.y.clone() + diag.x.clone() * diag.z.clone() + diag.y * diag.z) * 2
    }
}

impl<T> Bounds3<T>
where
    T: Num + Clone + Sub + Ord,
    <T as Sub>::Output: Num,
{
    /// method returns the index of which of the three axes is
    /// longest. This is useful, for example, when deciding which 
    /// axis to subdivide when building some of the ray-intersection 
    /// acceleration structures.
    pub fn maximum_extent(&self) -> usize {
        let diag = self.diagonal();
        if diag.x > diag.y && diag.x > diag.z {
            0
        } else if diag.y > diag.z {
            1
        } else {
            2
        }
    }
}

impl<T: Num + PartialOrd + Clone> Bounds3<T> {
    /// Static factory method to create a Bounding Box from two
    /// points representing corners of the Bunding Box.
    /// 
    /// Notice that the degenerate case where `p1 == p2` will result
    /// in the same result of calling `enclosing_point`, but the code
    /// will generate a lot more branches.
    pub fn new(p1: Point3<T>, p2: Point3<T>) -> Self {
        let min_x = if p1.x.lt(&p2.x) {
            p1.x.clone()
        } else {
            p2.x.clone()
        };
        let min_y = if p1.y.lt(&p2.z) {
            p1.y.clone()
        } else {
            p2.y.clone()
        };
        let min_z = if p1.z.lt(&p2.z) {
            p1.z.clone()
        } else {
            p2.z.clone()
        };
        let max_x = if p1.x.gt(&p2.x) {
            p1.x.clone()
        } else {
            p2.x.clone()
        };
        let max_y = if p1.y.gt(&p2.y) {
            p1.y.clone()
        } else {
            p2.y.clone()
        };
        let max_z = if p1.z.gt(&p2.z) {
            p1.z.clone()
        } else {
            p2.z.clone()
        };
        Bounds3 {
            min: Point3 {
                x: min_x,
                y: min_y,
                z: min_z,
            },
            max: Point3 {
                x: max_x,
                y: max_y,
                z: max_z,
            },
        }
    }
}

impl<T: Num> Index<usize> for Bounds3<T> {
    type Output = Point3<T>;

    /// Selects between p_min (index = 0) and p_max (index = 1).
    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 2);
        match index {
            0 => &self.min,
            1 => &self.max,
            _ => panic!("Index should be 0 <= i < 2"),
        }
    }
}

/// Given an Bounding Box, creates a new Bounding Box that contains the
/// original Bounding Box and the new point.
// TODO: make it a associate function? overload the `|` operator?
pub fn bound_point_union3<T>(bound: &Bounds3<T>, point: &Point3<T>) -> Bounds3<T>
where
    T: Num + Clone + PartialOrd,
{
    let min_x = if bound.min.x.lt(&point.x) {
        bound.min.x.clone()
    } else {
        point.x.clone()
    };
    let min_y = if bound.min.y.lt(&point.z) {
        bound.min.y.clone()
    } else {
        point.y.clone()
    };
    let min_z = if bound.min.z.lt(&point.z) {
        bound.min.z.clone()
    } else {
        point.z.clone()
    };
    let max_x = if bound.max.x.gt(&point.x) {
        bound.max.x.clone()
    } else {
        point.x.clone()
    };
    let max_y = if bound.max.y.gt(&point.y) {
        bound.max.y.clone()
    } else {
        point.y.clone()
    };
    let max_z = if bound.max.z.gt(&point.z) {
        bound.max.z.clone()
    } else {
        point.z.clone()
    };
    Bounds3::new(
        Point3 {
            x: min_x,
            y: min_y,
            z: min_z,
        },
        Point3 {
            x: max_x,
            y: max_y,
            z: max_z,
        },
    )
}

/// Given two bounding boxes, creates a new bounding box from the
/// that contains the original two. 
// TODO: make it a associate function? overload the `|` operator?
pub fn bounds_union3<T>(bound1: &Bounds3<T>, bound2: &Bounds3<T>) -> Bounds3<T>
where
    T: Num + Clone + PartialOrd,
{
    Bounds3 {
        min: Point3 {
            x: if bound1.min.x < bound2.min.x {
                bound1.min.x.clone()
            } else {
                bound2.min.x.clone()
            },
            y: if bound1.min.y < bound2.min.y {
                bound1.min.y.clone()
            } else {
                bound2.min.y.clone()
            },
            z: if bound1.min.z < bound2.min.z {
                bound1.min.z.clone()
            } else {
                bound2.min.z.clone()
            },
        },
        max: Point3 {
            x: if bound1.max.x > bound2.max.x {
                bound1.max.x.clone()
            } else {
                bound2.max.x.clone()
            },
            y: if bound1.max.y > bound2.max.y {
                bound1.max.y.clone()
            } else {
                bound2.max.y.clone()
            },
            z: if bound1.max.y > bound2.max.y {
                bound1.max.y.clone()
            } else {
                bound2.max.y.clone()
            },
        },
    }
}

/// Given two Bounding Boxes, returns the Bounding Box representing the
/// intersection between then. Returns `None` in case the Bounding Boxes 
/// do not overlap.
// TODO: make it a associate function? overload the `^` operator?
pub fn intersect<T>(bound1: &Bounds3<T>, bound2: &Bounds3<T>) -> Option<Bounds3<T>>
where
    T: Num + Clone + PartialOrd,
{
    if !overlaps(bound1, bound2) {
        None
    } else {
    Some(Bounds3 {
        min: Point3 {
            x: if bound1.min.x > bound2.min.x {
                bound1.min.x.clone()
            } else {
                bound2.min.x.clone()
            },
            y: if bound1.min.y > bound2.min.y {
                bound1.min.y.clone()
            } else {
                bound2.min.y.clone()
            },
            z: if bound1.min.z > bound2.min.z {
                bound1.min.z.clone()
            } else {
                bound2.min.z.clone()
            },
        },
        max: Point3 {
            x: if bound1.max.x < bound2.max.x {
                bound1.max.x.clone()
            } else {
                bound2.max.x.clone()
            },
            y: if bound1.max.y < bound2.max.y {
                bound1.max.y.clone()
            } else {
                bound2.max.y.clone()
            },
            z: if bound1.max.y < bound2.max.y {
                bound1.max.y.clone()
            } else {
                bound2.max.y.clone()
            },
        },
    })
    }
}

/// Checks if two Bounding Boxes overlap.
pub fn overlaps<T>(bound1: &Bounds3<T>, bound2: &Bounds3<T>) -> bool
where
    T: Num + PartialOrd,
{
    let x = (bound1.max.x >= bound2.min.x) && (bound1.min.x <= bound2.max.x);
    let y = (bound1.max.y >= bound2.min.y) && (bound1.min.y <= bound2.max.y);
    let z = (bound1.max.z >= bound2.min.z) && (bound1.min.z <= bound2.max.z);
    x && y && z
}

/// Checks if a point is contained inside the Bounding Box space
pub fn inside<T>(point: &Point3<T>, bound: &Bounds3<T>) -> bool
where
    T: Num + Ord,
{
    point.x >= bound.min.x
        && point.x <= bound.max.x
        && point.y >= bound.min.y
        && point.y <= bound.max.y
        && point.z >= bound.min.z
        && point.z <= bound.max.z
}

/// Checks if  a point is contained inside the Bounding Box space,
/// but doesn't consider points in te upper boundary to be inside
/// the bounds. According to the books it's mostly useful for 
/// integer typed data.
pub fn inside_exclusive<T>(point: &Point3<T>, bound: &Bounds3<T>) -> bool
where
    T: Num + Ord,
{
    point.x >= bound.min.x
        && point.x < bound.max.x
        && point.y >= bound.min.y
        && point.y < bound.max.y
        && point.z >= bound.min.z
        && point.z < bound.max.z
}

/// Expands the bounding box by a constant factor.
pub fn expand<T, W>(bound: Bounds3<T>, delta: W) -> Bounds3<<T as Sub<W>>::Output>
where
    T: Num + Add<W> + Clone + Sub<W>,
    W: Clone + Signed,
    <T as Sub<W>>::Output: Num + Ord + Clone,
    <T as Add<W>>::Output: Num + Ord + Clone + Into<<T as Sub<W>>::Output>,
    Point3<<T as Add<W>>::Output>: Into<Point3<<T as Sub<W>>::Output>>,
{
    Bounds3::new(
        bound.min - Vector3::<W> {
            x: delta.clone(),
            y: delta.clone(),
            z: delta.clone(),
        },
        (bound.max + Vector3::<W> {
            x: delta.clone(),
            y: delta.clone(),
            z: delta.clone(),
        }).into(),
    )
}

/// Linearly interpolates between the corners of the box by the
/// given amount in each dimension 
pub fn lerp<T, W>(
    bounds: Bounds3<W>,
    t: Point3<T>,
) -> Point3<<<W as Mul<<f64 as Sub<T>>::Output>>::Output as Add<<W as Mul<T>>::Output>>::Output>
where
    T: Float + Ord,
    W: Num + Mul<T> + Mul<<f64 as Sub<T>>::Output>,
    f64: Sub<T>,
    <W as Mul<<f64 as Sub<T>>::Output>>::Output: Add<<W as Mul<T>>::Output>,
    <<W as Mul<<f64 as Sub<T>>::Output>>::Output as Add<<W as Mul<T>>::Output>>::Output: Num,
{
    use math;
    Point3 {
        x: math::lerp(t.x, bounds.min.x, bounds.max.x),
        y: math::lerp(t.y, bounds.min.y, bounds.max.y),
        z: math::lerp(t.z, bounds.min.z, bounds.max.z),
    }
}

/// Returns the continuous position of a point relative to the corners
/// of the box, where a point a the minimum corner has offset `(0, 0, 0)`,
/// a point on the maximum corner has offset `(1, 1, 1)`.
pub fn offset<T>(bound: Bounds3<T>, p: Point3<T>) -> Vector3<T>
where
    T: Num + Ord + DivAssign + Sub + Clone,
{
    let mut o = p - bound.min.clone();
    if bound.max.x > bound.min.x {
        o.x /= bound.max.x - bound.min.x;
    }
    if bound.max.y > bound.min.y {
        o.y /= bound.max.y - bound.min.y;
    }
    if bound.max.z > bound.min.z {
        o.z /= bound.max.z - bound.min.z;
    }
    o
}


/// An itarator over every point of a bounding box. 
pub struct Bounds3Iterator<'a, T: Integer + Add + AddAssign + Clone + 'a> {
    p: Point3<T>,
    b: &'a Bounds3<T>,
}

/// An itarator over the corners of a bounding box. 
pub struct Corners3Iterator<'a, T: Num + Add + AddAssign + Clone + 'a> {
    cur_index: usize,
    data: &'a Corners3<T>
}

impl <'a, T: Num + Add + AddAssign + Clone + 'a> Iterator for Corners3Iterator<'a, T> {
    type Item = Point3<T>;

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        self.cur_index += 1;
        if self.cur_index >= 8 {
            None
        } else {
            Some(self.data.0[self.cur_index].clone()) 
        }
    }
}

impl<'a, T: Integer + Add + AddAssign + Clone + 'a> Iterator for Bounds3Iterator<'a, T> {
    type Item = Point3<T>;

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        self.p.x += T::one();
        if self.p.x == self.b.max.x {
            self.p.x = self.b.min.x.clone();
            self.p.y += T::one();
        }
        if self.p.y == self.b.max.y {
            self.p.y = self.b.min.y.clone();
            self.p.z += T::one();
        }
        if self.p.z == self.b.max.z {
            None
        } else {
            Some(self.p.clone())
        }
    }
}

impl<'a, T: Integer + Add + AddAssign + Sub + Clone + 'a> IntoIterator for &'a Bounds3<T> {
    type Item = Point3<T>;
    type IntoIter = Bounds3Iterator<'a, T>;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        Bounds3Iterator {
            p: Point3 {
                x: self.min.x.clone() - T::one(),
                y: self.min.y.clone(),
                z: self.min.z.clone(),
            },
            b: self,
        }
    }
}

impl<'a, T: Num + Add + AddAssign + Sub + Clone + 'a> IntoIterator for &'a Corners3<T> {
    type Item = Point3<T>;
    type IntoIter = Corners3Iterator<'a, T>;

    fn into_iter(self) -> <Self as IntoIterator>::IntoIter {
        Corners3Iterator {
            data: self,
            cur_index: 0
        }
    }
}