//! Bounds represents sub regions of space.
//! 
//! Bounds are used to divide space to facilitate hit detection
//! and parallelization of the tracing tasks by subdividing 
//! the space in smaller independent regions. 
//! 
//! We implement bounds for 2D and 3D space.
//! 

// TODO: Once we have const generics probably should find a way
// of having only one bound, parametrized by the dimension

mod bound2;
mod bound3;

pub use self::bound2::*;
pub use self::bound3::*;
