use math::matrix4::{transposed, Matrix4f};
use math::vectors::{dot3, Vector3f};
use std::ops::*;
use transform::Transform;

#[derive(Debug, Clone, Copy)]
pub struct Quarternion {
    v: Vector3f,
    w: f64,
}

impl<V: Into<Vector3f>, W: Into<f64>> From<(V, W)> for Quarternion {
    fn from(src: (V, W)) -> Quarternion {
        Quarternion {
            v: src.0.into(),
            w: src.1.into(),
        }
    }
}

impl Quarternion {
    pub fn get_v(&self) -> Vector3f {
        self.v
    }

    pub fn get_w(&self) -> f64 {
        self.w
    }

    pub fn to_transform(&self) -> Transform {
        let xx = self.v.x * self.v.x;
        let yy = self.v.y * self.v.y;
        let zz = self.v.z * self.v.z;
        let xy = self.v.x * self.v.y;
        let xz = self.v.x * self.v.z;
        let yz = self.v.y * self.v.z;
        let wx = self.v.x * self.w;
        let wy = self.v.y * self.w;
        let wz = self.v.z * self.w;

        let m: Matrix4f = [
            [1. - 2. * (yy + zz), 2. * (xy + wz), 2. * (xz - wy), 0.],
            [2. * (xy - wz), 1. - 2. * (xx + zz), 2. * (yz + wx), 0.],
            [2. * (xz + wy), 2. * (yz - wx), 1. - 2. * (xx + yy), 0.],
            [0., 0., 0., 0.],
        ].into();

        Transform::new_with_inv(transposed(&m), m)
    }

    pub fn from_transform(t: &Transform) -> Quarternion {
        let m = t.transformation_matrix();

        let trace = m[0][0] + m[1][1] + m[2][2];

        if trace > 0. {
            let s = (trace + 1.).sqrt();
            let w = s / 2.;
            let s = 0.5 / s;
            let v = Vector3f {
                x: (m[2][1] - m[1][2]) * s,
                y: (m[0][2] - m[2][0]) * s,
                z: (m[1][0] - m[0][1]) * s,
            };
            Quarternion { v, w }
        } else {
            let nxt = [1, 2, 3];
            let mut i = 0;
            if m[1][1] > m[0][0] {
                i = 1;
            }
            if m[2][2] > m[i][i] {
                i = 2;
            }
            let j = nxt[i];
            let k = nxt[j];
            let s = (m[i][i] - (m[j][j] + m[k][k])).sqrt() + 1.;
            let mut q = [0.; 3];
            q[i] = s * 0.5;
            let w = (m[k][j] - m[j][k]) * s;
            q[j] = (m[j][i] + m[i][j]) * s;
            q[k] = (m[k][i] + m[i][k]) * s;

            Quarternion { v: q.into(), w }
        }
    }
}

impl Into<Transform> for Quarternion {
    fn into(self) -> Transform {
        self.to_transform()
    }
}

impl Default for Quarternion {
    fn default() -> Self {
        Quarternion {
            v: Vector3f {
                x: 0.,
                y: 0.,
                z: 0.,
            },
            w: 1.,
        }
    }
}

impl Add for Quarternion {
    type Output = Quarternion;

    fn add(self, rhs: Quarternion) -> Quarternion {
        Quarternion {
            v: self.v + rhs.v,
            w: self.w + rhs.w,
        }
    }
}

impl AddAssign for Quarternion {
    fn add_assign(&mut self, rhs: Quarternion) {
        self.v += rhs.v;
        self.w += rhs.w;
    }
}

impl Sub for Quarternion {
    type Output = Quarternion;

    fn sub(self, rhs: Quarternion) -> Quarternion {
        Quarternion {
            v: self.v - rhs.v,
            w: self.w - rhs.w,
        }
    }
}

impl SubAssign for Quarternion {
    fn sub_assign(&mut self, rhs: Quarternion) {
        self.v -= rhs.v;
        self.w -= rhs.w;
    }
}

impl Mul<f64> for Quarternion {
    type Output = Quarternion;

    fn mul(self, rhs: f64) -> Quarternion {
        Quarternion {
            v: self.v * rhs,
            w: self.w * rhs,
        }
    }
}

impl MulAssign<f64> for Quarternion {
    fn mul_assign(&mut self, rhs: f64) {
        self.v *= rhs;
        self.w *= rhs;
    }
}

impl Div<f64> for Quarternion {
    type Output = Quarternion;

    fn div(self, rhs: f64) -> Quarternion {
        Quarternion {
            v: self.v / rhs,
            w: self.w / rhs,
        }
    }
}

impl DivAssign<f64> for Quarternion {
    fn div_assign(&mut self, rhs: f64) {
        self.v /= rhs;
        self.w /= rhs;
    }
}

impl Neg for Quarternion {
    type Output = Quarternion;

    fn neg(self) -> Self {
        Quarternion {
            v: -self.v,
            w: -self.w,
        }
    }
}

pub fn dot_quarternion(q1: Quarternion, q2: Quarternion) -> f64 {
    dot3(q1.v, q2.v) + q1.w * q2.w
}

pub fn normalize_quarternion(q: Quarternion) -> Quarternion {
    q / dot_quarternion(q, q)
}

pub fn slerp(t: f64, q1: Quarternion, q2: Quarternion) -> Quarternion {
    let cos_theta = dot_quarternion(q1, q2);
    if cos_theta > 0.9995 {
        normalize_quarternion(q1 * (1. - t) + q2 * t)
    } else {
        use utils::clamp;
        let theta = clamp(cos_theta, -1., 1.).acos();
        let thetap = theta * t;
        let qperp = normalize_quarternion(q2 - q1 * cos_theta);
        q1 * thetap.cos() + qperp * thetap.sin()
    }
}
