use std::ops::{Add, AddAssign, Div, DivAssign, Index, Mul, MulAssign, Neg, Sub, SubAssign};

use std::convert::{From, Into};

use num::{Float, Num, Signed};

use super::vectors::{self, Vector3, Vector3f};

use transform::{Transformable, Transform};

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Normal3<T: Num> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl Transformable for Normal3f {
    type Output = Self;

    fn apply_transform(&self, t: &Transform) -> Self::Output {
        let converted = t.inverser_transformation_matrix() * &Vector3f {
            x: self.x,
            y: self.y,
            z: self.z,
        };
        Normal3f {
            x: converted.x,
            y: converted.y,
            z: converted.z,
        }
    }
}


pub type Normal3f = Normal3<f64>;

impl<T: Mul + Float> Normal3<T> {
    pub fn length_squared(&self) -> T {
        (self.x.clone() * self.x.clone()
            + self.y.clone() * self.y.clone()
            + self.z.clone() * self.z.clone())
            .into()
    }
}

impl<T: Num> Into<Vector3<T>> for Normal3<T> {
    fn into(self) -> Vector3<T> {
        Vector3 {
            x: self.x,
            y: self.y,
            z: self.z,
        }
    }
}

impl<T: Float + Mul> Normal3<T> {
    pub fn length(&self) -> T {
        self.length_squared().sqrt()
    }
}

impl<T: Num> From<Vector3<T>> for Normal3<T> {
    fn from(src: Vector3<T>) -> Self {
        Normal3 {
            x: src.x,
            y: src.y,
            z: src.z,
        }
    }
}

impl<T: Num + Neg<Output = T>> Neg for Normal3<T> {
    type Output = Self;

    fn neg(self) -> <Self as Neg>::Output {
        Normal3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

pub fn normalize3<T>(norm: &Normal3<T>) -> Normal3<T>
where
    T: Float + Mul + Div,
{
    norm.clone() / norm.length()
}

impl<T: Num + Clone> Normal3<T> {
    pub fn casted<W>(&self) -> Normal3<W>
    where
        W: Num,
        T: Into<W>,
    {
        Normal3 {
            x: self.x.clone().into(),
            y: self.y.clone().into(),
            z: self.z.clone().into(),
        }
    }
}

pub fn max_component3<T: Num + Ord + Clone>(norm: &Normal3<T>) -> T {
    use std::cmp::max;
    max(norm.x.clone(), max(norm.y.clone(), norm.z.clone()))
}

pub fn min_component3<T: Num + Ord + Clone>(norm: &Normal3<T>) -> T {
    use std::cmp::min;
    min(norm.x.clone(), min(norm.y.clone(), norm.z.clone()))
}

pub fn max_dimension3<T: Num + PartialOrd + Clone>(norm: &Normal3<T>) -> usize {
    if norm.x.gt(&norm.y) {
        if norm.x.gt(&norm.z) {
            1
        } else {
            3
        }
    } else {
        if norm.y.gt(&norm.z) {
            2
        } else {
            3
        }
    }
}

pub fn component_wise_max3<T: Num + Ord + Clone>(
    norm1: &Normal3<T>,
    norm2: &Normal3<T>,
) -> Normal3<T> {
    use std::cmp::max;
    Normal3 {
        x: max(norm1.x.clone(), norm2.x.clone()),
        y: max(norm1.y.clone(), norm2.y.clone()),
        z: max(norm1.z.clone(), norm2.z.clone()),
    }
}

pub fn component_wise_min3<T: Num + Ord + Clone>(
    norm1: &Normal3<T>,
    norm2: &Normal3<T>,
) -> Normal3<T> {
    use std::cmp::min;
    Normal3 {
        x: min(norm1.x.clone(), norm2.x.clone()),
        y: min(norm1.y.clone(), norm2.y.clone()),
        z: min(norm1.z.clone(), norm2.z.clone()),
    }
}

pub fn permute3<T: Num + Clone>(
    norm: &Normal3<T>,
    permutation: (usize, usize, usize),
) -> Normal3<T> {
    assert!(permutation.0 < 3 && permutation.1 < 3 && permutation.2 < 3);

    Normal3 {
        x: norm[permutation.0].clone(),
        y: norm[permutation.1].clone(),
        z: norm[permutation.2].clone(),
    }
}

impl<T: Num> Index<usize> for Normal3<T> {
    type Output = T;

    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 3);
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => unreachable!(),
        }
    }
}

impl<T: Num + Add<Output = T>> Add for Normal3<T> {
    type Output = Normal3<T>;

    fn add(self, rhs: Self) -> <Self as Add>::Output {
        Normal3 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl<T: Num + AddAssign<T>> AddAssign for Normal3<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl<T: Num + Sub<Output = T>> Sub for Normal3<T> {
    type Output = Normal3<T>;

    fn sub(self, rhs: Self) -> <Self as Sub>::Output {
        Normal3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl<T: Num + SubAssign<T>> SubAssign for Normal3<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl<W: Num + Clone, T: Num + Mul<W, Output = W>> Mul<W> for Normal3<T> {
    type Output = Normal3<W>;

    fn mul(self, rhs: W) -> <Self as Mul<W>>::Output {
        Normal3 {
            x: self.x * rhs.clone(),
            y: self.y * rhs.clone(),
            z: self.z * rhs,
        }
    }
}

impl<T: Num + Clone, W: Num + MulAssign<T>> MulAssign<T> for Normal3<W> {
    fn mul_assign(&mut self, rhs: T) {
        self.x *= rhs.clone();
        self.y *= rhs.clone();
        self.z *= rhs.clone();
    }
}

impl<W: Num + Clone, T: Num + Div<W, Output = W>> Div<W> for Normal3<T> {
    type Output = Normal3<W>;

    fn div(self, rhs: W) -> <Self as Div<W>>::Output {
        assert!(rhs != W::zero());
        Normal3 {
            x: self.x / rhs.clone(),
            y: self.y / rhs.clone(),
            z: self.z / rhs,
        }
    }
}

impl<T: Num + Clone, W: Num + DivAssign<T>> DivAssign<T> for Normal3<W> {
    fn div_assign(&mut self, rhs: T) {
        self.x /= rhs.clone();
        self.y /= rhs.clone();
        self.z /= rhs;
    }
}

pub fn dot3<W, T>(first: &Normal3<T>, other: &Normal3<W>) -> W
where
    W: Num + Mul<T, Output = W> + Clone,
    T: Num + Clone,
{
    other.x.clone() * first.x.clone()
        + other.y.clone() * first.y.clone()
        + other.z.clone() * first.z.clone()
}

pub fn abs_dot3<W, T>(first: &Normal3<T>, other: &Normal3<W>) -> W
where
    W: Signed + Mul<T, Output = W> + Clone,
    T: Num + Clone,
{
    dot3(first, other).abs()
}

pub fn face_forward<T: Num + Clone + PartialOrd + Neg<Output = T> + Mul>(
    n: Normal3<T>,
    v: Vector3<T>,
) -> Normal3<T> {
    if vectors::dot3(n.clone().into(), v.clone()).le(&T::zero()) {
        -n
    } else {
        n
    }
}
