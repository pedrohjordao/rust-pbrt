//! This module exposes definitions for points in both 2D and 3D space.
//!
//! Once generic constants and specializations are stabilized we probably
//! will migrate this code be generic over dimensions.
//!
mod point2;
mod point3;

pub use self::point2::*;
pub use self::point3::*;
