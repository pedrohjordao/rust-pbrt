use num::Num;

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Point2<T: Num> {
    pub x: T,
    pub y: T,
}

pub type Point2i = Point2<i64>;
pub type Point2f = Point2<f64>;
