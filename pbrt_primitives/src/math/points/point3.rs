use std::convert::Into;
use std::ops::{Add, AddAssign, Index, Div, Mul, Neg, Sub, SubAssign};

use num::{Float, Num, Signed, Zero};

use super::Point2;
use math::vectors::Vector3;
use transform::{Transformable, Transform, TransformableWithError};

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Point3<T: Num> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl <T> Transformable for Point3<T>
where T: Num + Copy + Mul<f64>,
      f64: Mul<T>,
      <f64 as Mul<T>>::Output: Num,
      <f64 as Mul<T>>::Output: Num + Add + Add<f64> + Copy,
      <<f64 as Mul<T>>::Output as Add>::Output: Num + Add<f64> + Zero + Copy,
      <<<f64 as Mul<T>>::Output as Add>::Output as Add<f64>>::Output: Num + Div + Copy,
{
    type Output = Point3<<<f64 as Mul<T>>::Output as Add<f64>>::Output>;

    fn apply_transform(&self, t: &Transform) -> Self::Output {
        t.transformation_matrix() * self
    }
}

impl <T> TransformableWithError for Point3<T>
    where T: Num + Copy + Mul<f64>,
          f64: Mul<T>,
          <f64 as Mul<T>>::Output: Num,
          <f64 as Mul<T>>::Output: Num + Add + Add<f64> + Copy,
          <<f64 as Mul<T>>::Output as Add>::Output: Num + Add<f64> + Zero + Copy,
          <<<f64 as Mul<T>>::Output as Add>::Output as Add<f64>>::Output: Num + Div + Copy,
{
    type Error = Vector3<f64>;
    type ErrorComposition = Vector3<f64>;

    fn apply_transform_get_error(&self, t: &Transform) -> (<Self as Transformable>::Output, <Self as TransformableWithError>::Error) {
        unimplemented!()
    }

    fn apply_transform_compose_error(&self, v_error: <Self as TransformableWithError>::ErrorComposition, t: &Transform) -> (<Self as Transformable>::Output, <Self as TransformableWithError>::Error) {
        unimplemented!()
    }
}


pub type Point3i = Point3<i64>;
pub type Point3f = Point3<f64>;

impl<T: Num + Clone> Point3<T> {
    pub fn casted<W>(&self) -> Point3<W>
    where
        W: Num,
        T: Into<W>,
    {
        Point3 {
            x: self.x.clone().into(),
            y: self.y.clone().into(),
            z: self.z.clone().into(),
        }
    }
}

impl<T: Num> Index<usize> for Point3<T> {
    type Output = T;

    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 3);
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => unreachable!(),
        }
    }
}

impl<T> Into<Point2<T>> for Point3<T>
where
    T: Num,
{
    fn into(self) -> Point2<T> {
        Point2 {
            x: self.x,
            y: self.y,
        }
    }
}

impl<T, W> Into<Vector3<W>> for Point3<T>
where
    T: Num,
    W: Num + From<T>,
{
    fn into(self) -> Vector3<W> {
        Vector3 {
            x: self.x.into(),
            y: self.y.into(),
            z: self.z.into(),
        }
    }
}

impl<T, W> Add<Vector3<W>> for Point3<T>
where
    T: Num + Clone + Add<W>,
    W: Num + Clone,
    <T as Add<W>>::Output: Num,
{
    type Output = Point3<<T as Add<W>>::Output>;

    fn add(self, rhs: Vector3<W>) -> <Self as Add<Vector3<W>>>::Output {
        Point3 {
            x: self.x.clone() + rhs.x.clone(),
            y: self.y.clone() + rhs.y.clone(),
            z: self.z.clone() + rhs.z.clone(),
        }
    }
}

impl<T, W> AddAssign<Vector3<W>> for Point3<T>
where
    T: Num + Clone + AddAssign<W>,
    W: Num + Clone,
{
    fn add_assign(&mut self, rhs: Vector3<W>) {
        self.x += rhs.x.clone();
        self.y += rhs.y.clone();
        self.z += rhs.z.clone();
    }
}

impl<T, W> Sub<Vector3<W>> for Point3<T>
where
    T: Num + Clone + Sub<W>,
    W: Num + Clone,
    <T as Sub<W>>::Output: Num,
{
    type Output = Point3<<T as Sub<W>>::Output>;

    fn sub(self, rhs: Vector3<W>) -> <Self as Sub<Vector3<W>>>::Output {
        Point3 {
            x: self.x.clone() - rhs.x.clone(),
            y: self.y.clone() - rhs.y.clone(),
            z: self.z.clone() - rhs.z.clone(),
        }
    }
}

impl<T, W> SubAssign<Vector3<W>> for Point3<T>
where
    T: Num + Clone + SubAssign<W>,
    W: Num + Clone,
{
    fn sub_assign(&mut self, rhs: Vector3<W>) {
        self.x -= rhs.x.clone();
        self.y -= rhs.y.clone();
        self.z -= rhs.z.clone();
    }
}

impl<T> Sub for Point3<T>
where
    T: Num + Clone + Sub,
{
    type Output = Vector3<T>;

    fn sub(self, rhs: Point3<T>) -> <Self as Sub>::Output {
        Vector3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

pub fn point_distance3<T: Float + Sub>(p1: Point3<T>, p2: Point3<T>) -> T {
    (p1 - p2).length()
}

pub fn point_distance_squared3<T: Float + Sub>(p1: Point3<T>, p2: Point3<T>) -> T {
    (p1 - p2).length_squared()
}

pub fn lerp<T, W>(t: W, p1: Point3<T>, p2: Point3<T>) -> Point3<<T as Mul<W>>::Output>
where
    W: Float,
    T: Num + Mul<W> + Add,
    <T as Mul<W>>::Output: Num,
{
    assert!(t >= W::zero() && t <= W::one());
    let scaled_p1 = multiply_by_scalar3(p1, W::one() - t);
    let scaled_p2 = multiply_by_scalar3(p2, t);
    Point3 {
        x: scaled_p1.x + scaled_p2.x,
        y: scaled_p1.y + scaled_p2.y,
        z: scaled_p1.z + scaled_p2.z,
    }
}

pub fn component_wise_max3<T: Num + Ord + Clone>(vec1: &Point3<T>, vec2: &Point3<T>) -> Point3<T> {
    use std::cmp::max;
    Point3 {
        x: max(vec1.x.clone(), vec2.x.clone()),
        y: max(vec1.y.clone(), vec2.y.clone()),
        z: max(vec1.z.clone(), vec2.z.clone()),
    }
}

pub fn component_wise_min3<T: Num + Ord + Clone>(vec1: &Point3<T>, vec2: &Point3<T>) -> Point3<T> {
    use std::cmp::min;
    Point3 {
        x: min(vec1.x.clone(), vec2.x.clone()),
        y: min(vec1.y.clone(), vec2.y.clone()),
        z: min(vec1.z.clone(), vec2.z.clone()),
    }
}

pub fn permute3<T: Num + Clone>(
    permute: &Point3<T>,
    permutation: (usize, usize, usize),
) -> Point3<T> {
    assert!(permutation.0 < 3 && permutation.1 < 3 && permutation.2 < 3);

    Point3 {
        x: permute[permutation.0].clone(),
        y: permute[permutation.1].clone(),
        z: permute[permutation.2].clone(),
    }
}

pub fn floor<T: Float>(p: Point3<T>) -> Point3<T> {
    Point3 {
        x: p.x.floor(),
        y: p.y.floor(),
        z: p.z.floor(),
    }
}

pub fn ceil<T: Float>(p: Point3<T>) -> Point3<T> {
    Point3 {
        x: p.x.ceil(),
        y: p.y.ceil(),
        z: p.z.ceil(),
    }
}

pub fn abs<T: Float>(p: Point3<T>) -> Point3<T> {
    Point3 {
        x: p.x.abs(),
        y: p.y.abs(),
        z: p.z.abs(),
    }
}

fn multiply_by_scalar3<T, W>(p: Point3<T>, weight: W) -> Point3<<T as Mul<W>>::Output>
where
    T: Num + Mul<W>,
    W: Num + Clone,
    <T as Mul<W>>::Output: Num,
{
    Point3 {
        x: p.x * weight.clone(),
        y: p.y * weight.clone(),
        z: p.z * weight.clone(),
    }
}

impl<T: Signed> Neg for Point3<T> {
    type Output = Self;

    fn neg(self) -> <Self as Neg>::Output {
        Point3 {
            x: self.x.neg(),
            y: self.y.neg(),
            z: self.z.neg(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pont_distance() {
        let p1 = Point3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };
        let p2 = Point3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };
        assert_eq!(point_distance3(p1, p2), 0.0)
    }
}
