use std::ops::{Div, Mul};

use num::{Float, Num};

use super::vectors::{cross3, normalize3, Vector3};

pub struct CoordinateSystem<T: Num>(pub Vector3<T>, pub Vector3<T>, pub Vector3<T>);

impl<T> CoordinateSystem<T>
where
    T: Float + Mul + Div,
{
    pub fn from_vec(vec: Vector3<T>) -> CoordinateSystem<T> {
        let normalized = normalize3(&vec);
        let vec2;
        if normalized.x.abs() > normalized.y.abs() {
            vec2 = Vector3 {
                x: -normalized.z,
                y: T::zero(),
                z: normalized.x,
            } / (normalized.z.powi(2) + normalized.x.powi(2)).sqrt();
        } else {
            vec2 = Vector3 {
                x: T::zero(),
                y: normalized.z,
                z: -normalized.y,
            } / (normalized.y.powi(2) + normalized.z.powi(2)).sqrt();
        }

        let vec3 = cross3(&vec, &vec2);
        CoordinateSystem(vec, vec2, vec3)
    }
}
