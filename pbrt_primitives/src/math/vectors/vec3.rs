use std::convert::From;
use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};
use std::ops::{Div, DivAssign, Index, Neg};

use num::{Float, Num, Signed};
use num::{One, Zero};
use transform::{TransformableWithError, Transform, Transformable};

/// A vector in 3D space.
///
/// This struct is general over it's data type, as long as
/// it's a `Num`. That way we can have both discrete and floating
/// point vectors.
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Vector3<T: Num> {
    pub x: T,
    pub y: T,
    pub z: T,
}

pub type Vector3i = Vector3<i64>;
pub type Vector3f = Vector3<f64>;

impl<T: Num + Zero> Zero for Vector3<T> {

    /// Returns a vector with every field set to it's type parameter
    /// zero definition (so `i32`s will be `0`, `f32`s will be `0.0` and so on).
    fn zero() -> Self {
        Vector3 {
            x: T::zero(),
            y: T::zero(),
            z: T::zero(),
        }
    }

    /// Checks the vector is zero.
    ///
    /// This function will be `true` if every component of the
    /// vector is equal to it's own  `zero` definition.
    fn is_zero(&self) -> bool {
        self[0] == T::zero() && self[1] == T::zero() && self[2] == T::zero()
    }
}

impl<T: Num + One> One for Vector3<T> where
Self: Mul<Self, Output = Self>
{

    /// Returns a vector with every field set to it's type parameter
    /// one definition (so `i32`s will be `1`, `f32`s will be `1.0` and so on).
    fn one() -> Self {
        Vector3 {
            x: T::one(),
            y: T::one(),
            z: T::one(),
        }
    }

    /// Checks the vector is one.
    ///
    /// This function will be `true` if every component of the
    /// vector is equal to it's own  `one` definition.
    fn is_one(&self) -> bool {
        self[0] == T::one() && self[1] == T::one() && self[2] == T::one()
    }
}

impl<K: Num, T: Into<K> + Clone> From<[T; 3]> for Vector3<K> {

    /// Creates a `Vector3` from a 3 sized array.
    fn from(src: [T; 3]) -> Vector3<K> {
        Vector3 {
            x: src[0].clone().into(),
            y: src[1].clone().into(),
            z: src[2].clone().into(),
        }
    }
}

impl<T: Num, I: Into<T>, J: Into<T>, K: Into<T>> From<(I, J, K)> for Vector3<T> {
    /// Creates a `Vector3` from a 3 sized tuple of any type combination, as long
    /// as every type can be converted into `T`.
    fn from(src: (I, J, K)) -> Vector3<T> {
        Vector3 {
            x: src.0.into(),
            y: src.1.into(),
            z: src.2.into(),
        }
    }
}

/// Performs dot multiplication between two 3D vectors, resulting in a
/// new 3D vector.
///
/// Notice that the return type will be dictated by the second parameter.
pub fn dot3<W, T>(first: Vector3<T>, second: Vector3<W>) -> W
where
    W: Num + Mul<T, Output = W> + Clone,
    T: Num + Clone,
{
    second.x.clone() * first.x.clone()
        + second.y.clone() * first.y.clone()
        + second.z.clone() * first.z.clone()
}

/// For signed types returns a new Vector3 with every dimension set as
/// the absolute value of the original's vector dimensions.
pub fn abs_vec3<T>(src: Vector3<T>) -> Vector3<T>
where
    T: Signed
{
    Vector3 {
        x: src.x.abs(),
        y: src.y.abs(),
        z: src.z.abs()
    }
}

/// Help function that returns the absolute value of a `dot` product of
/// the two vectors.
pub fn abs_dot3<W, T>(first: Vector3<T>, other: Vector3<W>) -> W
where
    W: Signed + Mul<T, Output = W> + Clone,
    T: Num + Clone,
{
    dot3(first, other).abs()
}


/// Performs the cross product of two vectors.
///
/// Notice that the second parameter vector dictates this function's return type.
pub fn cross3<W, T>(first: &Vector3<T>, other: &Vector3<W>) -> Vector3<W>
where
    W: Num + Mul<T, Output = W> + Clone,
    T: Num + Mul<W, Output = W> + Clone,
{
    Vector3 {
        x: (first.y.clone() * other.z.clone()) - (other.y.clone() * first.z.clone()),
        y: (first.x.clone() * other.z.clone()) - (other.x.clone() * first.z.clone()),
        z: (first.y.clone() * other.x.clone()) - (other.y.clone() * first.x.clone()),
    }
}

impl<W, T> Mul<Vector3<T>> for Vector3<W>
where
    W: Num + Mul<T, Output = W> + Clone,
    T: Num + Mul<W, Output = W> + Clone,
{
    type Output = Vector3<W>;

    /// Performs the cross product of two vectors, where the result is dictated by
    /// the RHS type.
    fn mul(self, rhs: Vector3<T>) -> Vector3<W> {
        cross3(&rhs, &self)
    }
}

/// Returns a normalized (i.e. of length 1) vector based on the input parameter.
pub fn normalize3<T>(vec: &Vector3<T>) -> Vector3<T>
where
    T: Float + Mul + Div,
{
    vec.clone() / vec.length()
}

impl<T: Num + Clone> Vector3<T> {

    /// Generates a new vector of a different type as long
    /// as this Vector3 type parameter can be converted into
    /// the target type.
    pub fn casted<W>(&self) -> Vector3<W>
    where
        W: Num,
        T: Into<W>,
    {
        Vector3 {
            x: self.x.clone().into(),
            y: self.y.clone().into(),
            z: self.z.clone().into(),
        }
    }

    /// Static factory method that creates a new Vector3
    /// with it's `X` value set to `T::one` and `Y` and `Z` values
    /// set to `T::zero`
    pub fn x() -> Self {
        Vector3 {
            x: T::one(),
            y: T::zero(),
            z: T::zero(),
        }
    }

    /// Static factory method that creates a new Vector3
    /// with it's `Y` value set to `T::one` and `X` and `Z` values
    /// set to `T::zero`
    pub fn y() -> Self {
        Vector3 {
            x: T::zero(),
            y: T::one(),
            z: T::zero(),
        }
    }

    /// Static factory method that creates a new Vector3
    /// with it's `Z` value set to `T::one` and `Y` and `X` values
    /// set to `T::zero`
    pub fn z() -> Self {
        Vector3 {
            x: T::zero(),
            y: T::zero(),
            z: T::one(),
        }
    }
}

impl<T: Mul + Float> Vector3<T> {

    /// Returns the squared length of this Vector3.
    pub fn length_squared(&self) -> T {
        self.x.clone() * self.x.clone()
            + self.y.clone() * self.y.clone()
            + self.z.clone() * self.z.clone()
    }
}

impl<T: Float + Mul> Vector3<T> {

    /// Returns the length of this vector.
    ///
    /// Notice that this operation will first call `Vector3<T>::length_squared`.
    /// If you are calling this function to perform a squaring operation after you
    /// should call that function directly.
    pub fn length(&self) -> T {
        self.length_squared().sqrt()
    }
}

/// Returns the value of the largest component of this vector
pub fn max_component3<T: Num + Ord + Clone>(vec: &Vector3<T>) -> T {
    use std::cmp::max;
    max(vec.x.clone(), max(vec.y.clone(), vec.z.clone()))
}

/// Returns the value of the smallest component of this vector
pub fn min_component3<T: Num + Ord + Clone>(vec: &Vector3<T>) -> T {
    use std::cmp::min;
    min(vec.x.clone(), min(vec.y.clone(), vec.z.clone()))
}

/// Returns the index of the largest component of this vector
pub fn max_dimension3<T: Num + PartialOrd + Clone>(vec: &Vector3<T>) -> usize {
    if vec.x.gt(&vec.y) {
        if vec.x.gt(&vec.z) {
            1
        } else {
            3
        }
    } else {
        if vec.y.gt(&vec.z) {
            2
        } else {
            3
        }
    }
}

/// Returns a new Vector3 where each component is set to
/// the largest value of that component from the input parameters.
pub fn component_wise_max3<T: Num + Ord + Clone>(
    vec1: &Vector3<T>,
    vec2: &Vector3<T>,
) -> Vector3<T> {
    use std::cmp::max;
    Vector3 {
        x: max(vec1.x.clone(), vec2.x.clone()),
        y: max(vec1.y.clone(), vec2.y.clone()),
        z: max(vec1.z.clone(), vec2.z.clone()),
    }
}

/// Returns a new Vector3 where each component is set to
/// the smallest value of that component from the input parameters.
pub fn component_wise_min3<T: Num + Ord + Clone>(
    vec1: &Vector3<T>,
    vec2: &Vector3<T>,
) -> Vector3<T> {
    use std::cmp::min;
    Vector3 {
        x: min(vec1.x.clone(), vec2.x.clone()),
        y: min(vec1.y.clone(), vec2.y.clone()),
        z: min(vec1.z.clone(), vec2.z.clone()),
    }
}

/// Creates a new vector with a given permutation of the values of the original vector.
///
/// # Example
///
/// ```
/// use vectors::*;
///
/// let vec = Vector3 { x: 42.0, y: 69.0, z: 9.0 };
///
/// assert_eq!(permute3(vec, (1, 1, 2)), Vector3 { x: 42.0, y: 42.0, z: 69.0 });
/// ```
pub fn permute3<T: Num + Clone>(
    vec: &Vector3<T>,
    permutation: (usize, usize, usize),
) -> Vector3<T> {
    assert!(permutation.0 < 3 && permutation.1 < 3 && permutation.2 < 3);

    Vector3 {
        x: vec[permutation.0].clone(),
        y: vec[permutation.1].clone(),
        z: vec[permutation.2].clone(),
    }
}

impl<T: Num> Index<usize> for Vector3<T> {
    type Output = T;

    /// Indexes into the vector's components.
    ///
    /// Notice that index should be smaller than 3.
    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 3);
        match index {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => unreachable!(),
        }
    }
}

impl<T: Num + Add<Output = T>> Add for Vector3<T> {
    type Output = Vector3<T>;

    /// Performs a component-wise addition between two vectors.
    fn add(self, rhs: Self) -> <Self as Add>::Output {
        Vector3 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl<T: Num + AddAssign<T>> AddAssign for Vector3<T> {

    /// Reassigns each component of LHS to a component-wise addition.
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl<T: Num + Sub<Output = T>> Sub for Vector3<T> {
    type Output = Vector3<T>;

    /// Performs a component-wise subtraction between two vectors.
    fn sub(self, rhs: Self) -> <Self as Sub>::Output {
        Vector3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl<T: Num + SubAssign<T>> SubAssign for Vector3<T> {

    /// Reassigns each component of LHS to a component-wise subtraction.
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl<W: Num + Clone, T: Num + Mul<W, Output = W>> Mul<W> for Vector3<T> {
    type Output = Vector3<W>;

    /// Performs the multiplication of the vector by a value, as long
    /// as `T` implements `Mul<W>` .
    fn mul(self, rhs: W) -> <Self as Mul<W>>::Output {
        Vector3 {
            x: self.x * rhs.clone(),
            y: self.y * rhs.clone(),
            z: self.z * rhs,
        }
    }
}

impl<W: Num + Clone, T: Num + MulAssign<W>> MulAssign<W> for Vector3<T> {

    /// Reassigns each component of LHS to a component-wise multiplication,
    /// with similar rules as the `Mul<W>` implementation for  `Vector3`.
    fn mul_assign(&mut self, rhs: W) {
        self.x *= rhs.clone();
        self.y *= rhs.clone();
        self.z *= rhs.clone();
    }
}

impl<W: Num + Clone, T: Num + Div<W, Output = W>> Div<W> for Vector3<T> {
    type Output = Vector3<W>;

    /// Performs the division of the vector by a value, as long
    /// as `T` implements `Mul<W>` .
    fn div(self, rhs: W) -> <Self as Div<W>>::Output {
        assert!(rhs != W::zero());
        Vector3 {
            x: self.x / rhs.clone(),
            y: self.y / rhs.clone(),
            z: self.z / rhs,
        }
    }
}

impl<W: Num + Clone, T: Num + DivAssign<W>> DivAssign<W> for Vector3<T> {

    /// Reassigns each component of LHS to a component-wise division,
    /// with similar rules as the `Mul<W>` implementation for  `Vector3`.
    fn div_assign(&mut self, rhs: W) {
        self.x /= rhs.clone();
        self.y /= rhs.clone();
        self.z /= rhs;
    }
}

impl<T: Num + Neg<Output = T>> Neg for Vector3<T> {
    type Output = Self;

    /// Returns a vector with every value negated.
    ///
    /// As it would be expected, this only is possible if the
    /// vector's generic type can be negated.
    fn neg(self) -> <Self as Neg>::Output {
        Vector3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl <T> Transformable for Vector3<T>
    where T: Num + Copy + Mul<f64>,
          f64: Mul<T>,
          <f64 as Mul<T>>::Output: Num,
          <f64 as Mul<T>>::Output: Num + Add + Add<f64> + Copy,
          <<f64 as Mul<T>>::Output as Add>::Output: Num + Add<f64> + Zero + Copy,
          <<<f64 as Mul<T>>::Output as Add>::Output as Add<f64>>::Output: Num + Div + Copy,
{
    type Output = Vector3<<f64 as Mul<T>>::Output>;

    /// Transforms the vector according to some `Transform` value.
    fn apply_transform(&self, t: &Transform) -> Self::Output {
        t.transformation_matrix() * self
    }
}

// TODO: decidir o que fazer com isso
impl <T> TransformableWithError for Vector3<T>
    where T: Num + Copy + Mul<f64>,
          f64: Mul<T>,
          <f64 as Mul<T>>::Output: Num,
          <f64 as Mul<T>>::Output: Num + Add + Add<f64> + Copy,
          <<f64 as Mul<T>>::Output as Add>::Output: Num + Add<f64> + Zero + Copy,
          <<<f64 as Mul<T>>::Output as Add>::Output as Add<f64>>::Output: Num + Div + Copy,
{
    type Error = Vector3<f64>;
    type ErrorComposition = Vector3<f64>;

    fn apply_transform_get_error(&self, t: &Transform) -> (<Self as Transformable>::Output, <Self as TransformableWithError>::Error) {
        unimplemented!()
    }

    fn apply_transform_compose_error(&self, v_error: <Self as TransformableWithError>::ErrorComposition, t: &Transform) -> (<Self as Transformable>::Output, <Self as TransformableWithError>::Error) {
        unimplemented!()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cross_product3() {
        let first = Vector3f {
            x: 0.1,
            y: 0.1,
            z: 0.1,
        };
        let second = Vector3f {
            x: 0.1,
            y: 0.1,
            z: 0.1,
        };
        let third = Vector3i { x: 1, y: 1, z: 1 };

        cross3(&first, &second);
        cross3(&second, &first);
    }
}
