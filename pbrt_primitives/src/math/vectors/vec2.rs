use std::convert::From;
use std::ops::{Add, AddAssign, Mul, MulAssign, Sub, SubAssign};
use std::ops::{Div, DivAssign, Index};

use num::{Num, Signed, Zero};

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Vector2<T: Num> {
    pub x: T,
    pub y: T,
}

pub type Vector2i = Vector2<i64>;
pub type Vector2f = Vector2<f64>;

impl<T: Num + Add> Zero for Vector2<T> {
    fn zero() -> Self {
        Vector2 {
            x: T::zero(),
            y: T::zero(),
        }
    }

    fn is_zero(&self) -> bool {
        self[0] == T::zero() && self[1] == T::zero()
    }
}

//impl <T: Num + Mul + Clone> One for Vector2<T> {
//    fn one() -> Self {
//        Vector2 {
//            x: T::one(),
//            y: T::one(),
//        }
//    }
//
//    fn is_one(&self) -> bool {
//        self[0] == T::one() &&
//            self[1] == T::one()
//    }
//}

impl<K: Num, T: Clone + Into<K>> From<[T; 2]> for Vector2<K> {
    fn from(src: [T; 2]) -> Vector2<K> {
        Vector2 {
            x: src[0].clone().into(),
            y: src[1].clone().into(),
        }
    }
}

impl<T: Num, I: Into<T>, J: Into<T>> From<(I, J)> for Vector2<T> {
    fn from(src: (I, J)) -> Vector2<T> {
        Vector2 {
            x: src.0.into(),
            y: src.1.into(),
        }
    }
}

impl<T: Num + Clone> Vector2<T> {
    pub fn casted<W>(&self) -> Vector2<W>
    where
        W: Num,
        T: Into<W>,
    {
        Vector2 {
            x: self.x.clone().into(),
            y: self.y.clone().into(),
        }
    }

    pub fn x() -> Self {
        Vector2 {
            x: T::one(),
            y: T::zero(),
        }
    }

    pub fn y() -> Self {
        Vector2 {
            x: T::zero(),
            y: T::one(),
        }
    }
}

impl<T: Num + Mul + Clone> Vector2<T> {
    pub fn length_squared(&self) -> T {
        self.x.clone() * self.x.clone() + self.y.clone() * self.y.clone()
    }
}

impl<T: Num + Into<f64> + Mul + Clone> Vector2<T> {
    pub fn length(&self) -> f64 {
        let temp = self.casted::<f64>();
        temp.length_squared().sqrt()
    }
}

impl<T: Num + Add<Output = T>> Add for Vector2<T> {
    type Output = Vector2<T>;

    fn add(self, rhs: Self) -> <Self as Add>::Output {
        Vector2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<T: Num + AddAssign<T>> AddAssign for Vector2<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl<T: Num + Sub<Output = T>> Sub for Vector2<T> {
    type Output = Vector2<T>;

    fn sub(self, rhs: Self) -> <Self as Sub>::Output {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl<T: Num + SubAssign<T>> SubAssign for Vector2<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl<W: Num + Clone, T: Num + Mul<W, Output = W>> Mul<W> for Vector2<T> {
    type Output = Vector2<W>;

    fn mul(self, rhs: W) -> <Self as Mul<W>>::Output {
        Vector2 {
            x: self.x * rhs.clone(),
            y: self.y * rhs.clone(),
        }
    }
}

impl<T: Num + Clone, W: Num + MulAssign<T>> MulAssign<T> for Vector2<W> {
    fn mul_assign(&mut self, rhs: T) {
        self.x *= rhs.clone();
        self.y *= rhs;
    }
}

impl<W: Num + Clone, T: Num + Div<W, Output = W>> Div<W> for Vector2<T> {
    type Output = Vector2<W>;

    fn div(self, rhs: W) -> <Self as Div<W>>::Output {
        // TODO: Possible use 1/rhs as described in page 63?
        assert!(rhs != W::zero());
        Vector2 {
            x: self.x / rhs.clone(),
            y: self.y / rhs.clone(),
        }
    }
}
impl<T: Num + Clone, W: Num + DivAssign<T>> DivAssign<T> for Vector2<W> {
    fn div_assign(&mut self, rhs: T) {
        assert!(rhs != T::zero());
        self.x /= rhs.clone();
        self.y /= rhs.clone();
    }
}

pub fn dot2<W, T>(first: &Vector2<T>, other: &Vector2<W>) -> W
where
    W: Num + Mul<T, Output = W> + Clone,
    T: Num + Clone,
{
    other.x.clone() * first.x.clone() + other.y.clone() * first.y.clone()
}

pub fn abs_dot2<W, T>(first: &Vector2<T>, other: &Vector2<W>) -> W
where
    W: Signed + Mul<T, Output = W> + Clone,
    T: Num + Clone,
{
    dot2(first, other).abs()
}

pub fn normalize2<T>(vec: &Vector2<T>) -> Vector2<f64>
where
    T: Num + Mul + Div<f64, Output = f64> + Into<f64> + Clone,
{
    vec.clone() / vec.length()
}

pub fn max_component2<T: Num + Ord + Clone>(vec: &Vector2<T>) -> T {
    use std::cmp::max;
    max(vec.x.clone(), vec.y.clone())
}

pub fn min_component2<T: Num + Ord + Clone>(vec: &Vector2<T>) -> T {
    use std::cmp::min;
    min(vec.x.clone(), vec.y.clone())
}

pub fn max_dimension2<T: Num + PartialOrd + Clone>(vec: &Vector2<T>) -> usize {
    if vec.x.gt(&vec.y) {
        1
    } else {
        2
    }
}

pub fn component_wise_max2<T: Num + Ord + Clone>(
    vec1: &Vector2<T>,
    vec2: &Vector2<T>,
) -> Vector2<T> {
    use std::cmp::max;
    Vector2 {
        x: max(vec1.x.clone(), vec2.x.clone()),
        y: max(vec1.y.clone(), vec2.y.clone()),
    }
}

pub fn component_wise_min2<T: Num + Ord + Clone>(
    vec1: &Vector2<T>,
    vec2: &Vector2<T>,
) -> Vector2<T> {
    use std::cmp::min;
    Vector2 {
        x: min(vec1.x.clone(), vec2.x.clone()),
        y: min(vec1.y.clone(), vec2.y.clone()),
    }
}

pub fn permute2<T: Num + Clone>(
    vec: &Vector2<T>,
    permutation: (usize, usize, usize),
) -> Vector2<T> {
    assert!(permutation.0 < 2 && permutation.1 < 2);

    Vector2 {
        x: vec[permutation.0].clone(),
        y: vec[permutation.1].clone(),
    }
}

impl<T: Num> Index<usize> for Vector2<T> {
    type Output = T;

    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index < 2);
        match index {
            0 => &self.x,
            1 => &self.y,
            _ => unreachable!(),
        }
    }
}
