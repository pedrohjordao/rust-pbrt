//! This module exposes definitions for vector in both 2D and 3D space.
//!
//! Once generic constants and specializations are stabilized we probably
//! will migrate this code be generic over dimensions.

mod vec2;
mod vec3;

pub use self::vec2::*;
pub use self::vec3::*;
