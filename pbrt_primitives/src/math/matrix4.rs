use std::convert::From;
use std::ops::{Add, Index, IndexMut, Mul};

use num::Num;

pub type Matrix4f = Matrix4<f64>;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Matrix4<T: Num> {
    data: [[T; 4]; 4],
}

impl<T: Num> From<[[T; 4]; 4]> for Matrix4<T> {
    fn from(source: [[T; 4]; 4]) -> Self {
        Matrix4 { data: source }
    }
}

impl<T: Num> Matrix4<T> {
    pub fn new(
        t00: T,
        t01: T,
        t02: T,
        t03: T,
        t10: T,
        t11: T,
        t12: T,
        t13: T,
        t20: T,
        t21: T,
        t22: T,
        t23: T,
        t30: T,
        t31: T,
        t32: T,
        t33: T,
    ) -> Self {
        Matrix4 {
            data: [
                [t00, t01, t02, t03],
                [t10, t11, t12, t13],
                [t20, t21, t22, t23],
                [t30, t31, t32, t33],
            ],
        }
    }

    pub fn zero() -> Matrix4<T> {
        Matrix4::new(
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
        )
    }

    pub fn identity() -> Matrix4<T> {
        Matrix4::new(
            T::one(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::one(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::one(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::zero(),
            T::one(),
        )
    }
}

impl<T: Num> Index<usize> for Matrix4<T> {
    type Output = [T; 4];

    fn index(&self, index: usize) -> &<Self as Index<usize>>::Output {
        assert!(index >= 0 && index < 4);

        &self.data[index]
    }
}

impl<T: Num> IndexMut<usize> for Matrix4<T> {
    fn index_mut(&mut self, index: usize) -> &mut <Self as Index<usize>>::Output {
        assert!(index >= 0 && index < 4);

        &mut self.data[index]
    }
}

pub fn transposed<T: Num + Copy>(m: &Matrix4<T>) -> Matrix4<T> {
    Matrix4::new(
        m[0][0], m[1][0], m[2][0], m[3][0], m[0][1], m[1][1], m[2][1], m[3][1], m[0][2], m[1][2],
        m[2][2], m[3][2], m[0][3], m[1][3], m[2][3], m[3][3],
    )
}

use num::traits::float::Float;
use std::ops::{MulAssign, SubAssign};
pub fn inverse<T: Float + Clone + MulAssign + SubAssign>(
    m: &Matrix4<T>,
) -> Result<Matrix4<T>, String> {
    // Totaly stolen from https://github.com/wathiede/pbrt/blob/master/src/core/transform.rs,
    // But will eventualy replace with ndarrey
    let mut indxc: [usize; 4] = Default::default();
    let mut indxr: [usize; 4] = Default::default();
    let mut ipiv: [usize; 4] = Default::default();
    let mut minv = m.clone();

    for i in 0..4 {
        let mut irow: usize = 0;
        let mut icol: usize = 0;
        let mut big = T::zero();
        // Choose pivot
        for j in 0..4 {
            if ipiv[j] != 1 {
                for k in 0..4 {
                    if ipiv[k] == 0 {
                        if minv[j][k].abs() >= big {
                            big = minv[j][k].abs();
                            irow = j;
                            icol = k;
                        }
                    } else if ipiv[k] > 1 {
                        return Err("Singular matrix in MatrixInvert".to_string());
                    }
                }
            }
        }
        ipiv[icol] += 1;
        // Swap rows _irow_ and _icol_ for pivot
        if irow != icol {
            for k in 0..4 {
                let tmp = minv[irow][k];
                minv[irow][k] = minv[icol][k];
                minv[icol][k] = tmp;
            }
        }
        indxr[i] = irow;
        indxc[i] = icol;
        if minv[icol][icol] == T::zero() {
            return Err("Singular matrix in MatrixInvert".to_string());
        }

        // Set $m[icol][icol]$ to one by scaling row _icol_ appropriately
        let pivinv = minv[icol][icol].recip();
        minv[icol][icol] = T::one();
        for j in 0..4 {
            minv[icol][j] *= pivinv;
        }

        // Subtract this row from others to zero out their columns
        for j in 0..4 {
            if j != icol {
                let save = minv[j][icol];
                minv[j][icol] = T::zero();
                for k in 0..4 {
                    minv[j][k] -= minv[icol][k] * save;
                }
            }
        }
    }
    // Swap columns to reflect permutation
    for j in (0..4).rev() {
        if indxr[j] != indxc[j] {
            for k in 0..4 {
                let tmp = minv[k][indxr[j]];
                minv[k][indxr[j]] = minv[k][indxc[j]];
                minv[k][indxc[j]] = tmp;
            }
        }
    }
    Ok(Matrix4 { data: minv.data })
}

impl<T, W> Mul<Matrix4<T>> for Matrix4<W>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add,
    <<T as Mul<W>>::Output as Add>::Output: Num,
{
    type Output = Matrix4<<<T as Mul<W>>::Output as Add>::Output>;

    fn mul(self, other: Matrix4<T>) -> Self::Output {
        mul_matrix4(&other, &self)
    }
}

impl<'a, T, W> Mul<&'a Matrix4<T>> for Matrix4<W>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add,
    <<T as Mul<W>>::Output as Add>::Output: Num,
{
    type Output = Matrix4<<<T as Mul<W>>::Output as Add>::Output>;

    fn mul(self, other: &'a Matrix4<T>) -> Self::Output {
        mul_matrix4(other, &self)
    }
}

impl<'a, T, W> Mul<Matrix4<T>> for &'a Matrix4<W>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add,
    <<T as Mul<W>>::Output as Add>::Output: Num,
{
    type Output = Matrix4<<<T as Mul<W>>::Output as Add>::Output>;

    fn mul(self, other: Matrix4<T>) -> Self::Output {
        mul_matrix4(&other, self)
    }
}

impl<'a, 'b, T, W> Mul<&'a Matrix4<T>> for &'b Matrix4<W>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add,
    <<T as Mul<W>>::Output as Add>::Output: Num,
{
    type Output = Matrix4<<<T as Mul<W>>::Output as Add>::Output>;

    fn mul(self, other: &'a Matrix4<T>) -> Self::Output {
        mul_matrix4(other, self)
    }
}

fn mul_matrix4<T, W>(
    m1: &Matrix4<T>,
    m2: &Matrix4<W>,
) -> Matrix4<<<T as Mul<W>>::Output as Add>::Output>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add,
    <<T as Mul<W>>::Output as Add>::Output: Num,
{
    let mut zeroed = Matrix4::zero();
    for i in 0..4 {
        for j in 0..4 {
            zeroed[i][j] = m1[i][0] * m2[0][j]
                + m1[i][1] * m2[1][j]
                + m1[i][2] * m2[2][j]
                + m1[i][3] * m2[3][j];
        }
    }

    zeroed
}

use super::points::Point3;
use num::traits::Zero;
use std::ops::Div;
fn mul_matrix4_point3<T, W>(
    &m: &Matrix4<T>,
    p: &Point3<W>,
) -> Point3<<<T as Mul<W>>::Output as Add<T>>::Output>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Zero + Copy,
    <<<T as Mul<W>>::Output as Add>::Output as Add<T>>::Output: Num + Div + Copy,
{
    let res_x = m[0][0] * p.x + m[0][1] * p.y + m[0][2] * p.z + m[0][3];
    let res_y = m[1][0] * p.x + m[1][1] * p.y + m[1][2] * p.z + m[1][3];
    let res_z = m[2][0] * p.x + m[2][1] * p.y + m[2][2] * p.z + m[2][3];
    let res_w = m[3][0] * p.x + m[3][1] * p.y + m[3][2] * p.z + m[3][3];

    assert!(res_w != <<T as Mul<W>>::Output as Add<T>>::Output::zero());

    Point3 {
        x: res_x / res_w,
        y: res_y / res_w,
        z: res_z / res_w,
    }
}

impl<T, W> Mul<Point3<W>> for Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Zero + Copy,
    <<<T as Mul<W>>::Output as Add>::Output as Add<T>>::Output: Num + Div + Copy,
{
    type Output = Point3<<<T as Mul<W>>::Output as Add<T>>::Output>;

    fn mul(self, other: Point3<W>) -> Self::Output {
        mul_matrix4_point3(&self, &other)
    }
}

impl<'a, T, W> Mul<&'a Point3<W>> for Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Zero + Copy,
    <<<T as Mul<W>>::Output as Add>::Output as Add<T>>::Output: Num + Div + Copy,
{
    type Output = Point3<<<T as Mul<W>>::Output as Add<T>>::Output>;

    fn mul(self, other: &'a Point3<W>) -> Self::Output {
        mul_matrix4_point3(&self, other)
    }
}

impl<'a, T, W> Mul<Point3<W>> for &'a Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Zero + Copy,
    <<<T as Mul<W>>::Output as Add>::Output as Add<T>>::Output: Num + Div + Copy,
{
    type Output = Point3<<<T as Mul<W>>::Output as Add<T>>::Output>;

    fn mul(self, other: Point3<W>) -> Self::Output {
        mul_matrix4_point3(self, &other)
    }
}

impl<'b, 'a, T, W> Mul<&'b Point3<W>> for &'a Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Zero + Copy,
    <<<T as Mul<W>>::Output as Add>::Output as Add<T>>::Output: Num + Div + Copy,
{
    type Output = Point3<<<T as Mul<W>>::Output as Add<T>>::Output>;

    fn mul(self, other: &'b Point3<W>) -> Self::Output {
        mul_matrix4_point3(self, other)
    }
}

use super::vectors::Vector3 as Vec3;
fn mul_matrix4_vec3<T, W>(&m: &Matrix4<T>, p: &Vec3<W>) -> Vec3<<T as Mul<W>>::Output>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add<T>>::Output: Num + Copy,
{
    let res_x = m[0][0] * p.x + m[0][1] * p.y + m[0][2] * p.z;
    let res_y = m[1][0] * p.x + m[1][1] * p.y + m[1][2] * p.z;
    let res_z = m[2][0] * p.x + m[2][1] * p.y + m[2][2] * p.z;

    Vec3 {
        x: res_x,
        y: res_y,
        z: res_z,
    }
}

impl<T, W> Mul<Vec3<W>> for Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add<T>>::Output: Num + Copy,
{
    type Output = Vec3<<T as Mul<W>>::Output>;

    fn mul(self, other: Vec3<W>) -> Self::Output {
        mul_matrix4_vec3(&self, &other)
    }
}

impl<'a, T, W> Mul<&'a Vec3<W>> for Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add<T>>::Output: Num + Copy,
{
    type Output = Vec3<<T as Mul<W>>::Output>;

    fn mul(self, other: &'a Vec3<W>) -> Self::Output {
        mul_matrix4_vec3(&self, other)
    }
}

impl<'a, T, W> Mul<Vec3<W>> for &'a Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add<T>>::Output: Num + Copy,
{
    type Output = Vec3<<T as Mul<W>>::Output>;

    fn mul(self, other: Vec3<W>) -> Self::Output {
        mul_matrix4_vec3(self, &other)
    }
}

impl<'a, 'b, T, W> Mul<&'a Vec3<W>> for &'b Matrix4<T>
where
    T: Num + Copy + Mul<W>,
    W: Num + Copy,
    <T as Mul<W>>::Output: Num + Add + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add>::Output: Num + Add<T> + Copy,
    <<T as Mul<W>>::Output as Add<T>>::Output: Num + Copy,
{
    type Output = Vec3<<T as Mul<W>>::Output>;

    fn mul(self, other: &'a Vec3<W>) -> Self::Output {
        mul_matrix4_vec3(self, other)
    }
}
