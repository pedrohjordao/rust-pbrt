pub mod coordinate_system;
pub mod matrix4;
pub mod normals;
pub mod points;
pub mod quarternion;
pub mod vectors;

use std::ops::{Add, Mul, Sub};

use num::{Float, Num};

pub fn lerp<T, W>(
    t: T,
    v1: W,
    v2: W,
) -> <<W as Mul<<f64 as Sub<T>>::Output>>::Output as Add<<W as Mul<T>>::Output>>::Output
where
    T: Float,
    W: Num + Mul<<f64 as Sub<T>>::Output> + Mul<T>,
    f64: Sub<T>,
    <W as Mul<<f64 as Sub<T>>::Output>>::Output: Add<<W as Mul<T>>::Output>,
{
    assert!(t >= T::zero() && t <= T::one());
    v1 * (1.0 - t) + v2 * t
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Radians {
    val: f64,
}

impl Radians {
    #[inline]
    pub fn value(&self) -> f64 {
        self.val
    }

    #[inline]
    pub fn from_value(val: f64) -> Self {
        Radians { val }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct Degrees {
    val: f64,
}

impl Degrees {
    #[inline]
    pub fn value(&self) -> f64 {
        self.val
    }

    #[inline]
    pub fn from_value(val: f64) -> Self {
        Degrees { val }
    }
}

impl From<f64> for Radians {
    #[inline]
    fn from(val: f64) -> Radians {
        Radians { val }
    }
}

impl From<f64> for Degrees {
    #[inline]
    fn from(val: f64) -> Degrees {
        Degrees { val }
    }
}

impl From<Radians> for Degrees {
    #[inline]
    fn from(rad: Radians) -> Degrees {
        use std::f64::consts;

        Degrees {
            val: (180.0 / consts::PI) * rad.value(),
        }
    }
}

impl From<Degrees> for Radians {
    #[inline]
    fn from(deg: Degrees) -> Radians {
        use std::f64::consts;

        Radians {
            val: (consts::PI / 180.0) * deg.value(),
        }
    }
}
