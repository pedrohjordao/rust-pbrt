use bounds::Bounds2i;

pub struct Camera {
    film: Film,
}

impl Camera {
    pub fn new() -> Camera {
        Camera { film: Film::new() }
    }

    pub fn get_film(&self) -> &Film {
        &self.film
    }
}

pub struct Film {
    bounds: Bounds2i,
}

impl Film {
    pub fn new() -> Film {
        unimplemented!()
    }

    pub fn get_sample_bounds(&self) -> &Bounds2i {
        &self.bounds
    }
}
