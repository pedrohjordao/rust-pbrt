//! This crate contains the core structures and algorithms
//! in the pbrt-v3 book

extern crate num;
extern crate rayon;

mod integrators;
pub mod interactions;
mod lights;
pub mod scene;
mod utils;

pub mod bounds;
pub mod camera;
pub mod math;
pub mod medium;
pub mod rays;
pub mod samplers;
pub mod shapes;
pub mod transform;

// TODO: Probably old data, should be removed
pub struct Primitive;
pub struct SurfaceIntersection;

impl Primitive {
    pub fn world_bound(&self) -> bounds::Bounds3f {
        unimplemented!()
    }

    pub fn intersect(&self, ray: &rays::Ray) -> Option<SurfaceIntersection> {
        unimplemented!()
    }

    pub fn intersect_any(&self, ray: &rays::Ray) -> bool {
        unimplemented!()
    }
}

pub mod prelude {
    pub use lights::Light;
    pub use scene::Scene;
    pub use Primitive;
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {}
}
