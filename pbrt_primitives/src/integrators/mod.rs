pub mod sampler_integrator;

use super::samplers::{GeneratorKernel, Sampler};
use super::scene::Scene;

pub trait Integrator<T: GeneratorKernel> {
    fn render(&self, scene: &Scene);
    fn render_par(&self, scene: &Scene, tile_size: u8);
    fn preprocess(&self, scene: &Scene, sampler: &Sampler<T>);
}
