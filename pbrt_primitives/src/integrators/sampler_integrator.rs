use super::Integrator;
use super::Scene;
use camera::Camera;
use math::points::Point2i;
use rayon::prelude::*;
use samplers::{GeneratorKernel, Sampler};

pub struct SamplerIntegrator<T: GeneratorKernel> {
    sampler: Sampler<T>,
    camera: Camera,
}

impl<T: GeneratorKernel> SamplerIntegrator<T> {
    fn new(sampler: Sampler<T>, camera: Camera) -> SamplerIntegrator<T> {
        SamplerIntegrator { sampler, camera }
    }
}

impl<T: GeneratorKernel> Integrator<T> for SamplerIntegrator<T> {
    fn render(&self, scene: &Scene) {
        unimplemented!()
    }
    fn preprocess(&self, scene: &Scene, sampler: &Sampler<T>) {
        unimplemented!()
    }
    fn render_par(&self, scene: &Scene, tile_size: u8) {
        assert!(tile_size > 0);
        self.preprocess(scene, &self.sampler);
        let sample_bounds = self.camera.get_film().get_sample_bounds();
        let sample_extent = sample_bounds.diagonal();
        let tile_size: i64 = tile_size.into();
        let n_tiles = Point2i {
            x: sample_extent.x + tile_size - 1,
            y: sample_extent.y + tile_size - 1,
        };
        // split scene data and run with rayon using join
        scene
            .split_scene(16)
            .into_par_iter()
            .for_each(|s| unimplemented!());
        unimplemented!()
    }
}
