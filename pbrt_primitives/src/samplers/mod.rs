pub struct Sampler<T: GeneratorKernel>(T);

pub trait GeneratorKernel {
    fn sample(seed: i64);
}
