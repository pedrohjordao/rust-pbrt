pub mod sphere;

use std::rc::Rc;

use bounds::Bounds3f;
use transform::*;
use interactions::*;
use rays::*;

pub struct ShapeData {
    obj_to_world: Rc<Transform>,
    world_to_obj: Rc<Transform>,
    reverse_orientations: bool,
    transform_swaps_handness: bool,
}

impl ShapeData {
    pub fn new(
        obj_to_world: Rc<Transform>,
        world_to_obj: Rc<Transform>,
        reverse_orientations: bool,
    ) -> Self {
        ShapeData {
            obj_to_world: obj_to_world.clone(),
            world_to_obj: world_to_obj.clone(),
            reverse_orientations,
            transform_swaps_handness: obj_to_world.swaps_handness(),
        }
    }

    pub fn obj_to_world_transform(&self) -> Rc<Transform> {
        self.obj_to_world.clone()
    }

    pub fn world_to_obj_transform(&self) -> Rc<Transform> {
        self.world_to_obj.clone()
    }

    pub fn reverse_orientations(&self) -> bool {
        self.reverse_orientations
    }

    pub fn transform_swaps_handness(&self) -> bool {
        self.transform_swaps_handness
    }
}

pub trait Shape : Interactable {
    fn shape_data(&self) -> &ShapeData;
    fn object_bound(&self) -> &Bounds3f;
    fn get_intersection_data<'a>(&'a self, ray: &Ray, t_hit: f64, test_alpha_texture: bool) -> Option<SurfaceInteraction<'a, Self>>;
    fn area(&self) -> f64;

    fn intersects(&self, ray: &Ray, test_alpha_texture: bool) -> bool {
        let t_hit = ray.limit();
        self.get_intersection_data(ray, t_hit, test_alpha_texture).is_some()
    }

    fn world_bound(&self) -> Bounds3f {
        self.shape_data()
            .obj_to_world_transform()
            .apply(self.object_bound())
    }
}
