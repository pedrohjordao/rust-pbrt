use super::{Shape, ShapeData};
use bounds::Bounds3f;
use interactions::{Interactable, SurfaceInteraction};
use math::points::Point3f;
use math::{Degrees, Radians};
use rays::Ray;

pub struct Sphere {
    radius: f64,
    z_min: f64,
    z_max: f64,
    theta_min: f64,
    theta_max: f64,
    phi_max: Radians,
    data: ShapeData,
    obj_bound: Bounds3f,
}

impl Sphere {
    pub fn new(data: ShapeData, radius: f64, z_min: f64, z_max: f64, phi_max: Degrees) -> Self {
        use utils::clamp;
        let z_min = clamp(z_min.min(z_max), -radius, radius);
        let z_max = clamp(z_min.max(z_max), -radius, radius);
        let obj_bound = Bounds3f::new(
            Point3f {
                x: -radius,
                y: -radius,
                z: z_min,
            },
            Point3f {
                x: radius,
                y: radius,
                z: z_max,
            },
        );
        Sphere {
            radius,
            data,
            z_min,
            z_max,
            obj_bound,
            theta_min: clamp(z_min / radius, -1., 1.).acos(),
            theta_max: clamp(z_max / radius, -1., 1.).acos(),
            phi_max: Radians::from(clamp(phi_max.value(), 0., 360.)),
        }
    }
}

impl Interactable for Sphere {
    fn swap_handness(&self) -> bool {
        unimplemented!()
    }

    fn reverse_orientation(&self) -> bool {
        unimplemented!()
    }
}

impl Shape for Sphere {
    fn shape_data(&self) -> &ShapeData {
        &self.data
    }

    fn object_bound(&self) -> &Bounds3f {
        &self.obj_bound
    }

    fn get_intersection_data<'a>(
        &'a self,
        ray: &Ray,
        t_hit: f64,
        test_alpha_texture: bool,
    ) -> Option<SurfaceInteraction<'a, Self>> {
        // 1. transform ray to object space
        //
        // 2. compute quadratic sphere coeficients
        //
        // 3. solve quadratic equation for t values
        //
        // 4. compute sphere hit position and phi
        //
        // 5. test sphere intersection against clipping parameters
        //
        // 6. find parametric representation of sphere hit
        //
        // 7. compute error bounds for sphere intersection
        //
        // 8. initialize `SurfaceInteraction`from parametric information
        //
        // 9. update thit for quadratic intersection
        unimplemented!()
    }

    fn area(&self) -> f64 {
        unimplemented!()
    }
}
