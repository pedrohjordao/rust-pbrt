pub mod animated;

use bounds::Bounds3f;
use math::matrix4::*;
use math::points::Point3f;
use math::vectors::Vector3f as Vec3f;
use math::vectors::{cross3, normalize3};
use math::{Degrees, Radians};
use rays::RayDiferential;
use std::fmt;
use std::ops::Mul;
use utils::lazy::Lazy;

pub trait TransformableWithError: Transformable {
    type Error;
    type ErrorComposition;

    fn apply_transform_get_error(&self, t: &Transform) -> (Self::Output, Self::Error);

    fn apply_transform_compose_error(
        &self,
        v_error: Self::ErrorComposition,
        t: &Transform,
    ) -> (Self::Output, Self::Error);
}

pub trait Transformable {
    type Output;

    fn apply_transform(&self, t: &Transform) -> Self::Output;
}

impl Transformable for Transform {
    type Output = Self;

    fn apply_transform(&self, t: &Transform) -> <Self as Transformable>::Output {
        self * t
    }
}

pub struct Transform {
    m: Matrix4f,
    inv: Lazy<Matrix4f>,
}

impl PartialEq for Transform {
    fn eq(&self, rhs: &Transform) -> bool {
        self.m == rhs.m
    }
}

impl<T: Into<Matrix4f>> From<T> for Transform {
    fn from(src: T) -> Transform {
        Transform::new(src.into())
    }
}

impl fmt::Debug for Transform {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.m)
    }
}

impl Mul for Transform {
    type Output = Self;

    fn mul(self, rhs: Transform) -> Self::Output {
        Transform {
            m: self.m * rhs.m,
            inv: Lazy::new(move || *self.inv * *rhs.inv),
        }
    }
}

impl<'a, 'b> Mul<&'b Transform> for &'a Transform {
    type Output = Transform;

    fn mul(self, rhs: &'b Transform) -> Self::Output {
        let left = self.inv.clone();
        let right = rhs.inv.clone();
        Transform {
            m: self.m * rhs.m,
            inv: Lazy::new(move || left * right),
        }
    }
}

impl Default for Transform {
    fn default() -> Self {
        Transform {
            m: Matrix4f::identity(),
            inv: Lazy::new(|| Matrix4f::identity()),
        }
    }
}

impl Transform {
    pub fn swaps_handness(&self) -> bool {
        let det = self.m[0][0] * (self.m[1][1] * self.m[2][2] - self.m[1][2] * self.m[2][1])
            - self.m[0][1] * (self.m[1][0] * self.m[2][2] - self.m[1][2] * self.m[2][0])
            + self.m[0][2] * (self.m[1][0] * self.m[2][1] - self.m[1][1] * self.m[2][0]);

        det < 0.0
    }

    pub fn transformation_matrix(&self) -> &Matrix4f {
        &self.m
    }

    pub fn inverser_transformation_matrix(&self) -> &Matrix4f {
        use std::ops::Deref;
        &self.inv.deref()
    }

    pub fn new(m: Matrix4f) -> Transform {
        let m_cloned = m.clone();
        Transform {
            inv: Lazy::new(move || inverse(&m_cloned).unwrap()),
            m: m,
        }
    }

    pub fn new_with_inv(m: Matrix4f, inv: Matrix4f) -> Transform {
        Transform {
            m,
            inv: Lazy::new(move || inv),
        }
    }

    pub fn inverse(&self) -> Transform {
        let inner = self.m.clone();
        Transform {
            m: self.inv.clone(),
            inv: Lazy::new(move || inner),
        }
    }

    pub fn transpose(&self) -> Transform {
        let inv = self.inv.clone();
        Transform {
            m: transposed(&self.m),
            inv: Lazy::new(move || transposed(&inv)),
        }
    }

    pub fn is_identity(&self) -> bool {
        for i in 0..4 {
            for j in 0..4 {
                if i == j && self.m[i][j] != 1.0 {
                    return false;
                } else if self.m[i][j] != 0.0 {
                    return false;
                }
            }
        }
        true
    }

    pub fn new_translate(by: &Vec3f) -> Transform {
        let m: [[f64; 4]; 4] = [
            [1.0, 0.0, 0.0, by.x],
            [0.0, 1.0, 0.0, by.y],
            [0.0, 0.0, 1.0, by.z],
            [0.0, 0.0, 0.0, 0.0],
        ];
        let inv: [[f64; 4]; 4] = [
            [1.0, 0.0, 0.0, -by.x],
            [0.0, 1.0, 0.0, -by.y],
            [0.0, 0.0, 1.0, -by.z],
            [0.0, 0.0, 0.0, 0.0],
        ];
        Self::new_with_inv(m.into(), inv.into())
    }

    pub fn new_scaling(x: f64, y: f64, z: f64) -> Transform {
        assert!(x != 0.0 && y != 0.0 && z != 0.0);
        let m: [[f64; 4]; 4] = [
            [x, 0.0, 0.0, 0.0],
            [0.0, y, 0.0, 0.0],
            [0.0, 0.0, z, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ];
        let inv: [[f64; 4]; 4] = [
            [1.0 / x, 0.0, 0.0, 0.0],
            [0.0, 1.0 / y, 0.0, 0.0],
            [0.0, 0.0, 1.0 / z, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ];
        Self::new_with_inv(m.into(), inv.into())
    }

    pub fn new_rotation_around_x(theta: Degrees) -> Transform {
        let sin_theta = Radians::from(theta).value().sin();
        let cos_theta = Radians::from(theta).value().cos();
        let m: Matrix4f = [
            [1.0, 0.0, 0.0, 0.0],
            [0.0, cos_theta, -sin_theta, 0.0],
            [0.0, sin_theta, cos_theta, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ].into();
        let transposed = transposed(&m);
        Self::new_with_inv(m, transposed)
    }

    pub fn new_rotation_around_y(theta: Degrees) -> Transform {
        let sin_theta = Radians::from(theta).value().sin();
        let cos_theta = Radians::from(theta).value().cos();
        let m: Matrix4f = [
            [cos_theta, 0.0, sin_theta, 0.0],
            [0.0, 1.0, 0.0, 0.0],
            [-sin_theta, 0.0, cos_theta, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ].into();
        let transposed = transposed(&m);
        Self::new_with_inv(m, transposed)
    }

    pub fn new_rotation_around_z(theta: Degrees) -> Transform {
        let sin_theta = Radians::from(theta).value().sin();
        let cos_theta = Radians::from(theta).value().cos();
        let m: Matrix4f = [
            [cos_theta, -sin_theta, 0.0, 0.0],
            [sin_theta, cos_theta, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [0.0, 0.0, 0.0, 1.0],
        ].into();
        let transposed = transposed(&m);
        Self::new_with_inv(m, transposed)
    }

    pub fn new_rotation(theta: Degrees, axis: &Vec3f) -> Transform {
        let a = normalize3(axis);
        let sin_theta = Radians::from(theta).value().sin();
        let cos_theta = Radians::from(theta).value().cos();
        let mut m: Matrix4f = [
            [
                a.x * a.x + (1. - a.x * a.x) * cos_theta,
                a.x * a.y * (1. - cos_theta) - a.z * sin_theta,
                a.x * a.z * (1. - cos_theta) + a.y * sin_theta,
                0.,
            ],
            [
                a.x * a.y * (1. - cos_theta) + a.z * sin_theta,
                a.y * a.y + (1. - a.y * a.y) * cos_theta,
                a.y * a.z * (1. - cos_theta) - a.x * sin_theta,
                0.,
            ],
            [
                a.x * a.z * (1. - cos_theta) - a.y * sin_theta,
                a.y * a.z * (1. - cos_theta) + a.x * sin_theta,
                a.z * a.z + (1. - a.z * a.z) * cos_theta,
                0.,
            ],
            [0., 0., 0., 1.],
        ].into();
        let transposed = transposed(&m);
        Self::new_with_inv(m, transposed)
    }

    pub fn look_at(pos: &Point3f, look: &Point3f, up: &Vec3f) -> Self {
        let dir = normalize3(&(look.clone() - pos.clone()));
        let left = normalize3(&cross3(&normalize3(up), &dir));
        let new_up = cross3(&dir, &left);
        let m: Matrix4f = [
            [left.x, new_up.x, dir.x, pos.x],
            [left.y, new_up.y, dir.y, pos.y],
            [left.z, new_up.z, dir.z, pos.z],
            [0., 0., 0., 1.],
        ].into();
        Self::new_with_inv(inverse(&m).expect("matrix does not have an inverse"), m)
    }

    pub fn apply<T: Transformable>(&self, src: &T) -> T::Output {
        src.apply_transform(self)
    }

    pub fn apply_get_error<T: TransformableWithError>(&self, src: &T) -> (T::Output, T::Error) {
        src.apply_transform_get_error(self)
    }

    pub fn apply_compose_error<T: TransformableWithError>(
        &self,
        v_error: T::ErrorComposition,
        src: &T,
    ) -> (T::Output, T::Error) {
        src.apply_transform_compose_error(v_error, self)
    }
}
