use std::rc::Rc;

use super::*;
use math::vectors::Vector3f as Vec3f;
use math::quarternion::*;
use math::matrix4::*;
use rays::{Ray, RayDiferential};
use bounds::Bounds3f;

pub trait Animatable {
    type Output;

    fn apply_animation(&self, t: &AnimatedTransform) -> Self::Output;
}

impl Animatable for Ray {
    type Output = Ray;

    fn apply_animation(&self, t: &AnimatedTransform) -> <Self as Animatable>::Output {
        let dt = *self.time().expect("Untimed ray");
        if t.actually_animated ||
            dt <= t.start_time {
            t.start_transform.apply(self)
        } else if dt >= t.end_time {
            t.end_transform.apply(self)
        } else {
            t.interpolate(dt).apply(self)
        }
    }
}

impl Animatable for RayDiferential {
    type Output = RayDiferential;

    fn apply_animation(&self, t: &AnimatedTransform) -> <Self as Animatable>::Output {
        let dt = *self.time().expect("Untimed ray");
        if t.actually_animated ||
            dt <= t.start_time {
            t.start_transform.apply(self)
        } else if dt >= t.end_time {
            t.end_transform.apply(self)
        } else {
            t.interpolate(dt).apply(self)
        }
    }
}

impl <'a> Animatable for (f64, &'a Vec3f) {
    type Output = Vec3f;

    fn apply_animation(&self, t: &AnimatedTransform) -> <Self as Animatable>::Output {
        let dt = self.0;
        if t.actually_animated ||
            dt <= t.start_time {
            t.start_transform.apply(self.1)
        } else if dt >= t.end_time {
            t.end_transform.apply(self.1)
        } else {
            t.interpolate(dt).apply(self.1)
        }

    }
}

impl Animatable for (f64, Point3f) {
    type Output = Point3f;

    fn apply_animation(&self, t: &AnimatedTransform) -> <Self as Animatable>::Output {
        let dt = self.0;
        if t.actually_animated ||
            dt <= t.start_time {
            t.start_transform.apply(&self.1)
        } else if dt >= t.end_time {
            t.end_transform.apply(&self.1)
        } else {
            t.interpolate(dt).apply(&self.1)
        }
    }
}

impl <'a> Animatable for (f64, &'a Point3f) {
    type Output = Point3f;

    fn apply_animation(&self, t: &AnimatedTransform) -> <Self as Animatable>::Output {
        let dt = self.0;
        if t.actually_animated ||
            dt <= t.start_time {
            t.start_transform.apply(self.1)
        } else if dt >= t.end_time {
            t.end_transform.apply(self.1)
        } else {
            t.interpolate(dt).apply(self.1)
        }
    }
}

struct DerivativeTerm {
    kc: f64,
    kx: f64,
    ky: f64,
    kz: f64
}

impl DerivativeTerm {
    pub fn new(kc: f64, kx: f64, ky: f64, kz: f64) -> Self {
        DerivativeTerm {
            kc, kx, ky, kz
        }
    }

    pub fn eval(&self, p: &Point3f) -> f64 {
        self.kc + self.kx * p.x + self.ky * p.y + self.kz + p.z
    }
}

enum Rotation {
    HasRotation([[DerivativeTerm; 3]; 5]),
    NoRotation
}

pub struct AnimatedTransform {
    start_transform: Rc<Transform>,
    start_time: f64,
    end_transform: Rc<Transform>,
    end_time: f64,
    actually_animated: bool,
    t: [Vec3f; 2],
    r: [Quarternion; 2],
    s: [Matrix4f; 2],
    rotation: Rotation,
}

impl AnimatedTransform {
    #[allow(unused_parens)]
    pub fn new(start: Rc<Transform>, start_time: f64,
                end: Rc<Transform>, end_time: f64) -> Self {
        let (t1, mut r1, s1) = decompose(&start.transformation_matrix());
        let (t2, r2, s2) = decompose(&end.transformation_matrix());

        if dot_quarternion(r1, r2) < 0. {
            r1 = -r2;
        }

        let r1 = r1;
        let actually_animated = *start != *end;

        let has_rotation = dot_quarternion(r1, r2) < 0.9995;
        // compute terms o motion derivative function
        if has_rotation {
            use utils::clamp;
            let cos_theta = dot_quarternion(r1, r2);
            let theta = clamp(cos_theta, -1., 1.);
            let qperp = normalize_quarternion(r2 - r1 * cos_theta);

              let t0x = t1.x;
            let t0y = t1.y;
            let t0z = t1.z;
            let t1x = t1.x;
            let t1y = t1.y;
            let t1z = t1.z;
            let q1x = r1.get_v().x;
            let q1y = r1.get_v().y;
            let q1z = r1.get_v().z;
            let q1w = r1.get_w();
            let qperpx = qperp.get_v().x;
            let qperpy = qperp.get_v().y;
            let qperpz = qperp.get_v().z;
            let qperpw = qperp.get_w();
            let s000 = s1[0][0];
            let s001 = s1[0][1];
            let s002 = s1[0][2];
            let s010 = s1[1][0];
            let s011 = s1[1][1];
            let s012 = s1[1][2];
            let s020 = s1[2][0];
            let s021 = s1[2][1];
            let s022 = s1[2][2];
            let s100 = s2[0][0];
            let s101 = s2[0][1];
            let s102 = s2[0][2];
            let s110 = s2[1][0];
            let s111 = s2[1][1];
            let s112 = s2[1][2];
            let s120 = s2[2][0];
            let s121 = s2[2][1];
            let s122 = s2[2][2];

            let c10 = DerivativeTerm::new(
                -t0x + t1x,
                (-1. + q1y * q1y + q1z * q1z + qperpy * qperpy + qperpz * qperpz) *
                    s000 +
                    q1w * q1z * s010 - qperpx * qperpy * s010 +
                    qperpw * qperpz * s010 - q1w * q1y * s020 -
                    qperpw * qperpy * s020 - qperpx * qperpz * s020 + s100 -
                    q1y * q1y * s100 - q1z * q1z * s100 - qperpy * qperpy * s100 -
                    qperpz * qperpz * s100 - q1w * q1z * s110 +
                    qperpx * qperpy * s110 - qperpw * qperpz * s110 +
                    q1w * q1y * s120 + qperpw * qperpy * s120 +
                    qperpx * qperpz * s120 +
                    q1x * (-(q1y * s010) - q1z * s020 + q1y * s110 + q1z * s120),
                (-1. + q1y * q1y + q1z * q1z + qperpy * qperpy + qperpz * qperpz) *
                    s001 +
                    q1w * q1z * s011 - qperpx * qperpy * s011 +
                    qperpw * qperpz * s011 - q1w * q1y * s021 -
                    qperpw * qperpy * s021 - qperpx * qperpz * s021 + s101 -
                    q1y * q1y * s101 - q1z * q1z * s101 - qperpy * qperpy * s101 -
                    qperpz * qperpz * s101 - q1w * q1z * s111 +
                    qperpx * qperpy * s111 - qperpw * qperpz * s111 +
                    q1w * q1y * s121 + qperpw * qperpy * s121 +
                    qperpx * qperpz * s121 +
                    q1x * (-(q1y * s011) - q1z * s021 + q1y * s111 + q1z * s121),
                (-1. + q1y * q1y + q1z * q1z + qperpy * qperpy + qperpz * qperpz) *
                    s002 +
                    q1w * q1z * s012 - qperpx * qperpy * s012 +
                    qperpw * qperpz * s012 - q1w * q1y * s022 -
                    qperpw * qperpy * s022 - qperpx * qperpz * s022 + s102 -
                    q1y * q1y * s102 - q1z * q1z * s102 - qperpy * qperpy * s102 -
                    qperpz * qperpz * s102 - q1w * q1z * s112 +
                    qperpx * qperpy * s112 - qperpw * qperpz * s112 +
                    q1w * q1y * s122 + qperpw * qperpy * s122 +
                    qperpx * qperpz * s122 +
                    q1x * (-(q1y * s012) - q1z * s022 + q1y * s112 + q1z * s122));

            let c20 = DerivativeTerm::new(
                0.,
                -(qperpy * qperpy * s000) - qperpz * qperpz * s000 +
                    qperpx * qperpy * s010 - qperpw * qperpz * s010 +
                    qperpw * qperpy * s020 + qperpx * qperpz * s020 +
                    q1y * q1y * (s000 - s100) + q1z * q1z * (s000 - s100) +
                    qperpy * qperpy * s100 + qperpz * qperpz * s100 -
                    qperpx * qperpy * s110 + qperpw * qperpz * s110 -
                    qperpw * qperpy * s120 - qperpx * qperpz * s120 +
                    2. * q1x * qperpy * s010 * theta -
                    2. * q1w * qperpz * s010 * theta +
                    2. * q1w * qperpy * s020 * theta +
                    2. * q1x * qperpz * s020 * theta +
                    q1y *
                        (q1x * (-s010 + s110) + q1w * (-s020 + s120) +
                            2. * (-2. * qperpy * s000 + qperpx * s010 + qperpw * s020) *
                                theta) +
                    q1z * (q1w * (s010 - s110) + q1x * (-s020 + s120) -
                        2. * (2. * qperpz * s000 + qperpw * s010 - qperpx * s020) *
                            theta),
                -(qperpy * qperpy * s001) - qperpz * qperpz * s001 +
                    qperpx * qperpy * s011 - qperpw * qperpz * s011 +
                    qperpw * qperpy * s021 + qperpx * qperpz * s021 +
                    q1y * q1y * (s001 - s101) + q1z * q1z * (s001 - s101) +
                    qperpy * qperpy * s101 + qperpz * qperpz * s101 -
                    qperpx * qperpy * s111 + qperpw * qperpz * s111 -
                    qperpw * qperpy * s121 - qperpx * qperpz * s121 +
                    2. * q1x * qperpy * s011 * theta -
                    2. * q1w * qperpz * s011 * theta +
                    2. * q1w * qperpy * s021 * theta +
                    2. * q1x * qperpz * s021 * theta +
                    q1y *
                        (q1x * (-s011 + s111) + q1w * (-s021 + s121) +
                            2. * (-2. * qperpy * s001 + qperpx * s011 + qperpw * s021) *
                                theta) +
                    q1z * (q1w * (s011 - s111) + q1x * (-s021 + s121) -
                        2. * (2. * qperpz * s001 + qperpw * s011 - qperpx * s021) *
                            theta),
                -(qperpy * qperpy * s002) - qperpz * qperpz * s002 +
                    qperpx * qperpy * s012 - qperpw * qperpz * s012 +
                    qperpw * qperpy * s022 + qperpx * qperpz * s022 +
                    q1y * q1y * (s002 - s102) + q1z * q1z * (s002 - s102) +
                    qperpy * qperpy * s102 + qperpz * qperpz * s102 -
                    qperpx * qperpy * s112 + qperpw * qperpz * s112 -
                    qperpw * qperpy * s122 - qperpx * qperpz * s122 +
                    2. * q1x * qperpy * s012 * theta -
                    2. * q1w * qperpz * s012 * theta +
                    2. * q1w * qperpy * s022 * theta +
                    2. * q1x * qperpz * s022 * theta +
                    q1y *
                        (q1x * (-s012 + s112) + q1w * (-s022 + s122) +
                            2. * (-2. * qperpy * s002 + qperpx * s012 + qperpw * s022) *
                                theta) +
                    q1z * (q1w * (s012 - s112) + q1x * (-s022 + s122) -
                        2. * (2. * qperpz * s002 + qperpw * s012 - qperpx * s022) *
                            theta));

            let c30 = DerivativeTerm::new(
                0.,
                -2. * (q1x * qperpy * s010 - q1w * qperpz * s010 +
                    q1w * qperpy * s020 + q1x * qperpz * s020 -
                    q1x * qperpy * s110 + q1w * qperpz * s110 -
                    q1w * qperpy * s120 - q1x * qperpz * s120 +
                    q1y * (-2. * qperpy * s000 + qperpx * s010 + qperpw * s020 +
                        2. * qperpy * s100 - qperpx * s110 - qperpw * s120) +
                    q1z * (-2. * qperpz * s000 - qperpw * s010 + qperpx * s020 +
                        2. * qperpz * s100 + qperpw * s110 - qperpx * s120)) *
                    theta,
                -2. * (q1x * qperpy * s011 - q1w * qperpz * s011 +
                    q1w * qperpy * s021 + q1x * qperpz * s021 -
                    q1x * qperpy * s111 + q1w * qperpz * s111 -
                    q1w * qperpy * s121 - q1x * qperpz * s121 +
                    q1y * (-2. * qperpy * s001 + qperpx * s011 + qperpw * s021 +
                        2. * qperpy * s101 - qperpx * s111 - qperpw * s121) +
                    q1z * (-2. * qperpz * s001 - qperpw * s011 + qperpx * s021 +
                        2. * qperpz * s101 + qperpw * s111 - qperpx * s121)) *
                    theta,
                -2. * (q1x * qperpy * s012 - q1w * qperpz * s012 +
                    q1w * qperpy * s022 + q1x * qperpz * s022 -
                    q1x * qperpy * s112 + q1w * qperpz * s112 -
                    q1w * qperpy * s122 - q1x * qperpz * s122 +
                    q1y * (-2. * qperpy * s002 + qperpx * s012 + qperpw * s022 +
                        2. * qperpy * s102 - qperpx * s112 - qperpw * s122) +
                    q1z * (-2. * qperpz * s002 - qperpw * s012 + qperpx * s022 +
                        2. * qperpz * s102 + qperpw * s112 - qperpx * s122)) *
                    theta);

            let c40 = DerivativeTerm::new(
                0.,
                -(q1x * qperpy * s010) + q1w * qperpz * s010 - q1w * qperpy * s020 -
                    q1x * qperpz * s020 + q1x * qperpy * s110 -
                    q1w * qperpz * s110 + q1w * qperpy * s120 +
                    q1x * qperpz * s120 + 2. * q1y * q1y * s000 * theta +
                    2. * q1z * q1z * s000 * theta -
                    2. * qperpy * qperpy * s000 * theta -
                    2. * qperpz * qperpz * s000 * theta +
                    2. * qperpx * qperpy * s010 * theta -
                    2. * qperpw * qperpz * s010 * theta +
                    2. * qperpw * qperpy * s020 * theta +
                    2. * qperpx * qperpz * s020 * theta +
                    q1y * (-(qperpx * s010) - qperpw * s020 +
                        2. * qperpy * (s000 - s100) + qperpx * s110 +
                        qperpw * s120 - 2. * q1x * s010 * theta -
                        2. * q1w * s020 * theta) +
                    q1z * (2. * qperpz * s000 + qperpw * s010 - qperpx * s020 -
                        2. * qperpz * s100 - qperpw * s110 + qperpx * s120 +
                        2. * q1w * s010 * theta - 2. * q1x * s020 * theta),
                -(q1x * qperpy * s011) + q1w * qperpz * s011 - q1w * qperpy * s021 -
                    q1x * qperpz * s021 + q1x * qperpy * s111 -
                    q1w * qperpz * s111 + q1w * qperpy * s121 +
                    q1x * qperpz * s121 + 2. * q1y * q1y * s001 * theta +
                    2. * q1z * q1z * s001 * theta -
                    2. * qperpy * qperpy * s001 * theta -
                    2. * qperpz * qperpz * s001 * theta +
                    2. * qperpx * qperpy * s011 * theta -
                    2. * qperpw * qperpz * s011 * theta +
                    2. * qperpw * qperpy * s021 * theta +
                    2. * qperpx * qperpz * s021 * theta +
                    q1y * (-(qperpx * s011) - qperpw * s021 +
                        2. * qperpy * (s001 - s101) + qperpx * s111 +
                        qperpw * s121 - 2. * q1x * s011 * theta -
                        2. * q1w * s021 * theta) +
                    q1z * (2. * qperpz * s001 + qperpw * s011 - qperpx * s021 -
                        2. * qperpz * s101 - qperpw * s111 + qperpx * s121 +
                        2. * q1w * s011 * theta - 2. * q1x * s021 * theta),
                -(q1x * qperpy * s012) + q1w * qperpz * s012 - q1w * qperpy * s022 -
                    q1x * qperpz * s022 + q1x * qperpy * s112 -
                    q1w * qperpz * s112 + q1w * qperpy * s122 +
                    q1x * qperpz * s122 + 2. * q1y * q1y * s002 * theta +
                    2. * q1z * q1z * s002 * theta -
                    2. * qperpy * qperpy * s002 * theta -
                    2. * qperpz * qperpz * s002 * theta +
                    2. * qperpx * qperpy * s012 * theta -
                    2. * qperpw * qperpz * s012 * theta +
                    2. * qperpw * qperpy * s022 * theta +
                    2. * qperpx * qperpz * s022 * theta +
                    q1y * (-(qperpx * s012) - qperpw * s022 +
                        2. * qperpy * (s002 - s102) + qperpx * s112 +
                        qperpw * s122 - 2. * q1x * s012 * theta -
                        2. * q1w * s022 * theta) +
                    q1z * (2. * qperpz * s002 + qperpw * s012 - qperpx * s022 -
                        2. * qperpz * s102 - qperpw * s112 + qperpx * s122 +
                        2. * q1w * s012 * theta - 2. * q1x * s022 * theta));

            let c50 = DerivativeTerm::new(
                0.,
                2. * (qperpy * qperpy * s000 + qperpz * qperpz * s000 -
                    qperpx * qperpy * s010 + qperpw * qperpz * s010 -
                    qperpw * qperpy * s020 - qperpx * qperpz * s020 -
                    qperpy * qperpy * s100 - qperpz * qperpz * s100 +
                    q1y * q1y * (-s000 + s100) + q1z * q1z * (-s000 + s100) +
                    qperpx * qperpy * s110 - qperpw * qperpz * s110 +
                    q1y * (q1x * (s010 - s110) + q1w * (s020 - s120)) +
                    qperpw * qperpy * s120 + qperpx * qperpz * s120 +
                    q1z * (-(q1w * s010) + q1x * s020 + q1w * s110 - q1x * s120)) *
                    theta,
                2. * (qperpy * qperpy * s001 + qperpz * qperpz * s001 -
                    qperpx * qperpy * s011 + qperpw * qperpz * s011 -
                    qperpw * qperpy * s021 - qperpx * qperpz * s021 -
                    qperpy * qperpy * s101 - qperpz * qperpz * s101 +
                    q1y * q1y * (-s001 + s101) + q1z * q1z * (-s001 + s101) +
                    qperpx * qperpy * s111 - qperpw * qperpz * s111 +
                    q1y * (q1x * (s011 - s111) + q1w * (s021 - s121)) +
                    qperpw * qperpy * s121 + qperpx * qperpz * s121 +
                    q1z * (-(q1w * s011) + q1x * s021 + q1w * s111 - q1x * s121)) *
                    theta,
                2. * (qperpy * qperpy * s002 + qperpz * qperpz * s002 -
                    qperpx * qperpy * s012 + qperpw * qperpz * s012 -
                    qperpw * qperpy * s022 - qperpx * qperpz * s022 -
                    qperpy * qperpy * s102 - qperpz * qperpz * s102 +
                    q1y * q1y * (-s002 + s102) + q1z * q1z * (-s002 + s102) +
                    qperpx * qperpy * s112 - qperpw * qperpz * s112 +
                    q1y * (q1x * (s012 - s112) + q1w * (s022 - s122)) +
                    qperpw * qperpy * s122 + qperpx * qperpz * s122 +
                    q1z * (-(q1w * s012) + q1x * s022 + q1w * s112 - q1x * s122)) *
                    theta);

            let c11 = DerivativeTerm::new(
                -t0y + t1y,
                -(qperpx * qperpy * s000) - qperpw * qperpz * s000 - s010 +
                    q1z * q1z * s010 + qperpx * qperpx * s010 +
                    qperpz * qperpz * s010 - q1y * q1z * s020 +
                    qperpw * qperpx * s020 - qperpy * qperpz * s020 +
                    qperpx * qperpy * s100 + qperpw * qperpz * s100 +
                    q1w * q1z * (-s000 + s100) + q1x * q1x * (s010 - s110) + s110 -
                    q1z * q1z * s110 - qperpx * qperpx * s110 -
                    qperpz * qperpz * s110 +
                    q1x * (q1y * (-s000 + s100) + q1w * (s020 - s120)) +
                    q1y * q1z * s120 - qperpw * qperpx * s120 +
                    qperpy * qperpz * s120,
                -(qperpx * qperpy * s001) - qperpw * qperpz * s001 - s011 +
                    q1z * q1z * s011 + qperpx * qperpx * s011 +
                    qperpz * qperpz * s011 - q1y * q1z * s021 +
                    qperpw * qperpx * s021 - qperpy * qperpz * s021 +
                    qperpx * qperpy * s101 + qperpw * qperpz * s101 +
                    q1w * q1z * (-s001 + s101) + q1x * q1x * (s011 - s111) + s111 -
                    q1z * q1z * s111 - qperpx * qperpx * s111 -
                    qperpz * qperpz * s111 +
                    q1x * (q1y * (-s001 + s101) + q1w * (s021 - s121)) +
                    q1y * q1z * s121 - qperpw * qperpx * s121 +
                    qperpy * qperpz * s121,
                -(qperpx * qperpy * s002) - qperpw * qperpz * s002 - s012 +
                    q1z * q1z * s012 + qperpx * qperpx * s012 +
                    qperpz * qperpz * s012 - q1y * q1z * s022 +
                    qperpw * qperpx * s022 - qperpy * qperpz * s022 +
                    qperpx * qperpy * s102 + qperpw * qperpz * s102 +
                    q1w * q1z * (-s002 + s102) + q1x * q1x * (s012 - s112) + s112 -
                    q1z * q1z * s112 - qperpx * qperpx * s112 -
                    qperpz * qperpz * s112 +
                    q1x * (q1y * (-s002 + s102) + q1w * (s022 - s122)) +
                    q1y * q1z * s122 - qperpw * qperpx * s122 +
                    qperpy * qperpz * s122);

            let c21 = DerivativeTerm::new(
                0.,
                qperpx * qperpy * s000 + qperpw * qperpz * s000 + q1z * q1z * s010 -
                    qperpx * qperpx * s010 - qperpz * qperpz * s010 -
                    q1y * q1z * s020 - qperpw * qperpx * s020 +
                    qperpy * qperpz * s020 - qperpx * qperpy * s100 -
                    qperpw * qperpz * s100 + q1x * q1x * (s010 - s110) -
                    q1z * q1z * s110 + qperpx * qperpx * s110 +
                    qperpz * qperpz * s110 + q1y * q1z * s120 +
                    qperpw * qperpx * s120 - qperpy * qperpz * s120 +
                    2. * q1z * qperpw * s000 * theta +
                    2. * q1y * qperpx * s000 * theta -
                    4. * q1z * qperpz * s010 * theta +
                    2. * q1z * qperpy * s020 * theta +
                    2. * q1y * qperpz * s020 * theta +
                    q1x * (q1w * s020 + q1y * (-s000 + s100) - q1w * s120 +
                        2. * qperpy * s000 * theta - 4. * qperpx * s010 * theta -
                        2. * qperpw * s020 * theta) +
                    q1w * (-(q1z * s000) + q1z * s100 + 2. * qperpz * s000 * theta -
                        2. * qperpx * s020 * theta),
                qperpx * qperpy * s001 + qperpw * qperpz * s001 + q1z * q1z * s011 -
                    qperpx * qperpx * s011 - qperpz * qperpz * s011 -
                    q1y * q1z * s021 - qperpw * qperpx * s021 +
                    qperpy * qperpz * s021 - qperpx * qperpy * s101 -
                    qperpw * qperpz * s101 + q1x * q1x * (s011 - s111) -
                    q1z * q1z * s111 + qperpx * qperpx * s111 +
                    qperpz * qperpz * s111 + q1y * q1z * s121 +
                    qperpw * qperpx * s121 - qperpy * qperpz * s121 +
                    2. * q1z * qperpw * s001 * theta +
                    2. * q1y * qperpx * s001 * theta -
                    4. * q1z * qperpz * s011 * theta +
                    2. * q1z * qperpy * s021 * theta +
                    2. * q1y * qperpz * s021 * theta +
                    q1x * (q1w * s021 + q1y * (-s001 + s101) - q1w * s121 +
                        2. * qperpy * s001 * theta - 4. * qperpx * s011 * theta -
                        2. * qperpw * s021 * theta) +
                    q1w * (-(q1z * s001) + q1z * s101 + 2. * qperpz * s001 * theta -
                        2. * qperpx * s021 * theta),
                qperpx * qperpy * s002 + qperpw * qperpz * s002 + q1z * q1z * s012 -
                    qperpx * qperpx * s012 - qperpz * qperpz * s012 -
                    q1y * q1z * s022 - qperpw * qperpx * s022 +
                    qperpy * qperpz * s022 - qperpx * qperpy * s102 -
                    qperpw * qperpz * s102 + q1x * q1x * (s012 - s112) -
                    q1z * q1z * s112 + qperpx * qperpx * s112 +
                    qperpz * qperpz * s112 + q1y * q1z * s122 +
                    qperpw * qperpx * s122 - qperpy * qperpz * s122 +
                    2. * q1z * qperpw * s002 * theta +
                    2. * q1y * qperpx * s002 * theta -
                    4. * q1z * qperpz * s012 * theta +
                    2. * q1z * qperpy * s022 * theta +
                    2. * q1y * qperpz * s022 * theta +
                    q1x * (q1w * s022 + q1y * (-s002 + s102) - q1w * s122 +
                        2. * qperpy * s002 * theta - 4. * qperpx * s012 * theta -
                        2. * qperpw * s022 * theta) +
                    q1w * (-(q1z * s002) + q1z * s102 + 2. * qperpz * s002 * theta -
                        2. * qperpx * s022 * theta));

            let c31 = DerivativeTerm::new(
                0., 2. * (-(q1x * qperpy * s000) - q1w * qperpz * s000 +
                    2. * q1x * qperpx * s010 + q1x * qperpw * s020 +
                    q1w * qperpx * s020 + q1x * qperpy * s100 +
                    q1w * qperpz * s100 - 2. * q1x * qperpx * s110 -
                    q1x * qperpw * s120 - q1w * qperpx * s120 +
                    q1z * (2. * qperpz * s010 - qperpy * s020 +
                        qperpw * (-s000 + s100) - 2. * qperpz * s110 +
                        qperpy * s120) +
                    q1y * (-(qperpx * s000) - qperpz * s020 + qperpx * s100 +
                        qperpz * s120)) *
                    theta,
                2. * (-(q1x * qperpy * s001) - q1w * qperpz * s001 +
                    2. * q1x * qperpx * s011 + q1x * qperpw * s021 +
                    q1w * qperpx * s021 + q1x * qperpy * s101 +
                    q1w * qperpz * s101 - 2. * q1x * qperpx * s111 -
                    q1x * qperpw * s121 - q1w * qperpx * s121 +
                    q1z * (2. * qperpz * s011 - qperpy * s021 +
                        qperpw * (-s001 + s101) - 2. * qperpz * s111 +
                        qperpy * s121) +
                    q1y * (-(qperpx * s001) - qperpz * s021 + qperpx * s101 +
                        qperpz * s121)) *
                    theta,
                2. * (-(q1x * qperpy * s002) - q1w * qperpz * s002 +
                    2. * q1x * qperpx * s012 + q1x * qperpw * s022 +
                    q1w * qperpx * s022 + q1x * qperpy * s102 +
                    q1w * qperpz * s102 - 2. * q1x * qperpx * s112 -
                    q1x * qperpw * s122 - q1w * qperpx * s122 +
                    q1z * (2. * qperpz * s012 - qperpy * s022 +
                        qperpw * (-s002 + s102) - 2. * qperpz * s112 +
                        qperpy * s122) +
                    q1y * (-(qperpx * s002) - qperpz * s022 + qperpx * s102 +
                        qperpz * s122)) *
                    theta);

            let c41 = DerivativeTerm::new(
                0.,
                -(q1x * qperpy * s000) - q1w * qperpz * s000 +
                    2. * q1x * qperpx * s010 + q1x * qperpw * s020 +
                    q1w * qperpx * s020 + q1x * qperpy * s100 +
                    q1w * qperpz * s100 - 2. * q1x * qperpx * s110 -
                    q1x * qperpw * s120 - q1w * qperpx * s120 +
                    2. * qperpx * qperpy * s000 * theta +
                    2. * qperpw * qperpz * s000 * theta +
                    2. * q1x * q1x * s010 * theta + 2. * q1z * q1z * s010 * theta -
                    2. * qperpx * qperpx * s010 * theta -
                    2. * qperpz * qperpz * s010 * theta +
                    2. * q1w * q1x * s020 * theta -
                    2. * qperpw * qperpx * s020 * theta +
                    2. * qperpy * qperpz * s020 * theta +
                    q1y * (-(qperpx * s000) - qperpz * s020 + qperpx * s100 +
                        qperpz * s120 - 2. * q1x * s000 * theta) +
                    q1z * (2. * qperpz * s010 - qperpy * s020 +
                        qperpw * (-s000 + s100) - 2. * qperpz * s110 +
                        qperpy * s120 - 2. * q1w * s000 * theta -
                        2. * q1y * s020 * theta),
                -(q1x * qperpy * s001) - q1w * qperpz * s001 +
                    2. * q1x * qperpx * s011 + q1x * qperpw * s021 +
                    q1w * qperpx * s021 + q1x * qperpy * s101 +
                    q1w * qperpz * s101 - 2. * q1x * qperpx * s111 -
                    q1x * qperpw * s121 - q1w * qperpx * s121 +
                    2. * qperpx * qperpy * s001 * theta +
                    2. * qperpw * qperpz * s001 * theta +
                    2. * q1x * q1x * s011 * theta + 2. * q1z * q1z * s011 * theta -
                    2. * qperpx * qperpx * s011 * theta -
                    2. * qperpz * qperpz * s011 * theta +
                    2. * q1w * q1x * s021 * theta -
                    2. * qperpw * qperpx * s021 * theta +
                    2. * qperpy * qperpz * s021 * theta +
                    q1y * (-(qperpx * s001) - qperpz * s021 + qperpx * s101 +
                        qperpz * s121 - 2. * q1x * s001 * theta) +
                    q1z * (2. * qperpz * s011 - qperpy * s021 +
                        qperpw * (-s001 + s101) - 2. * qperpz * s111 +
                        qperpy * s121 - 2. * q1w * s001 * theta -
                        2. * q1y * s021 * theta),
                -(q1x * qperpy * s002) - q1w * qperpz * s002 +
                    2. * q1x * qperpx * s012 + q1x * qperpw * s022 +
                    q1w * qperpx * s022 + q1x * qperpy * s102 +
                    q1w * qperpz * s102 - 2. * q1x * qperpx * s112 -
                    q1x * qperpw * s122 - q1w * qperpx * s122 +
                    2. * qperpx * qperpy * s002 * theta +
                    2. * qperpw * qperpz * s002 * theta +
                    2. * q1x * q1x * s012 * theta + 2. * q1z * q1z * s012 * theta -
                    2. * qperpx * qperpx * s012 * theta -
                    2. * qperpz * qperpz * s012 * theta +
                    2. * q1w * q1x * s022 * theta -
                    2. * qperpw * qperpx * s022 * theta +
                    2. * qperpy * qperpz * s022 * theta +
                    q1y * (-(qperpx * s002) - qperpz * s022 + qperpx * s102 +
                        qperpz * s122 - 2. * q1x * s002 * theta) +
                    q1z * (2. * qperpz * s012 - qperpy * s022 +
                        qperpw * (-s002 + s102) - 2. * qperpz * s112 +
                        qperpy * s122 - 2. * q1w * s002 * theta -
                        2. * q1y * s022 * theta));

            let c51 = DerivativeTerm::new(
                0., -2. * (qperpx * qperpy * s000 + qperpw * qperpz * s000 +
                    q1z * q1z * s010 - qperpx * qperpx * s010 -
                    qperpz * qperpz * s010 - q1y * q1z * s020 -
                    qperpw * qperpx * s020 + qperpy * qperpz * s020 -
                    qperpx * qperpy * s100 - qperpw * qperpz * s100 +
                    q1w * q1z * (-s000 + s100) + q1x * q1x * (s010 - s110) -
                    q1z * q1z * s110 + qperpx * qperpx * s110 +
                    qperpz * qperpz * s110 +
                    q1x * (q1y * (-s000 + s100) + q1w * (s020 - s120)) +
                    q1y * q1z * s120 + qperpw * qperpx * s120 -
                    qperpy * qperpz * s120) *
                    theta,
                -2. * (qperpx * qperpy * s001 + qperpw * qperpz * s001 +
                    q1z * q1z * s011 - qperpx * qperpx * s011 -
                    qperpz * qperpz * s011 - q1y * q1z * s021 -
                    qperpw * qperpx * s021 + qperpy * qperpz * s021 -
                    qperpx * qperpy * s101 - qperpw * qperpz * s101 +
                    q1w * q1z * (-s001 + s101) + q1x * q1x * (s011 - s111) -
                    q1z * q1z * s111 + qperpx * qperpx * s111 +
                    qperpz * qperpz * s111 +
                    q1x * (q1y * (-s001 + s101) + q1w * (s021 - s121)) +
                    q1y * q1z * s121 + qperpw * qperpx * s121 -
                    qperpy * qperpz * s121) *
                    theta,
                -2. * (qperpx * qperpy * s002 + qperpw * qperpz * s002 +
                    q1z * q1z * s012 - qperpx * qperpx * s012 -
                    qperpz * qperpz * s012 - q1y * q1z * s022 -
                    qperpw * qperpx * s022 + qperpy * qperpz * s022 -
                    qperpx * qperpy * s102 - qperpw * qperpz * s102 +
                    q1w * q1z * (-s002 + s102) + q1x * q1x * (s012 - s112) -
                    q1z * q1z * s112 + qperpx * qperpx * s112 +
                    qperpz * qperpz * s112 +
                    q1x * (q1y * (-s002 + s102) + q1w * (s022 - s122)) +
                    q1y * q1z * s122 + qperpw * qperpx * s122 -
                    qperpy * qperpz * s122) *
                    theta);

            let c12 = DerivativeTerm::new(
                -t0z + t1z, (qperpw * qperpy * s000 - qperpx * qperpz * s000 -
                    q1y * q1z * s010 - qperpw * qperpx * s010 -
                    qperpy * qperpz * s010 - s020 + q1y * q1y * s020 +
                    qperpx * qperpx * s020 + qperpy * qperpy * s020 -
                    qperpw * qperpy * s100 + qperpx * qperpz * s100 +
                    q1x * q1z * (-s000 + s100) + q1y * q1z * s110 +
                    qperpw * qperpx * s110 + qperpy * qperpz * s110 +
                    q1w * (q1y * (s000 - s100) + q1x * (-s010 + s110)) +
                    q1x * q1x * (s020 - s120) + s120 - q1y * q1y * s120 -
                    qperpx * qperpx * s120 - qperpy * qperpy * s120),
                (qperpw * qperpy * s001 - qperpx * qperpz * s001 -
                    q1y * q1z * s011 - qperpw * qperpx * s011 -
                    qperpy * qperpz * s011 - s021 + q1y * q1y * s021 +
                    qperpx * qperpx * s021 + qperpy * qperpy * s021 -
                    qperpw * qperpy * s101 + qperpx * qperpz * s101 +
                    q1x * q1z * (-s001 + s101) + q1y * q1z * s111 +
                    qperpw * qperpx * s111 + qperpy * qperpz * s111 +
                    q1w * (q1y * (s001 - s101) + q1x * (-s011 + s111)) +
                    q1x * q1x * (s021 - s121) + s121 - q1y * q1y * s121 -
                    qperpx * qperpx * s121 - qperpy * qperpy * s121),
                (qperpw * qperpy * s002 - qperpx * qperpz * s002 -
                    q1y * q1z * s012 - qperpw * qperpx * s012 -
                    qperpy * qperpz * s012 - s022 + q1y * q1y * s022 +
                    qperpx * qperpx * s022 + qperpy * qperpy * s022 -
                    qperpw * qperpy * s102 + qperpx * qperpz * s102 +
                    q1x * q1z * (-s002 + s102) + q1y * q1z * s112 +
                    qperpw * qperpx * s112 + qperpy * qperpz * s112 +
                    q1w * (q1y * (s002 - s102) + q1x * (-s012 + s112)) +
                    q1x * q1x * (s022 - s122) + s122 - q1y * q1y * s122 -
                    qperpx * qperpx * s122 - qperpy * qperpy * s122));

            let c22 = DerivativeTerm::new(
                0.,
                (q1w * q1y * s000 - q1x * q1z * s000 - qperpw * qperpy * s000 +
                    qperpx * qperpz * s000 - q1w * q1x * s010 - q1y * q1z * s010 +
                    qperpw * qperpx * s010 + qperpy * qperpz * s010 +
                    q1x * q1x * s020 + q1y * q1y * s020 - qperpx * qperpx * s020 -
                    qperpy * qperpy * s020 - q1w * q1y * s100 + q1x * q1z * s100 +
                    qperpw * qperpy * s100 - qperpx * qperpz * s100 +
                    q1w * q1x * s110 + q1y * q1z * s110 - qperpw * qperpx * s110 -
                    qperpy * qperpz * s110 - q1x * q1x * s120 - q1y * q1y * s120 +
                    qperpx * qperpx * s120 + qperpy * qperpy * s120 -
                    2. * q1y * qperpw * s000 * theta + 2. * q1z * qperpx * s000 * theta -
                    2. * q1w * qperpy * s000 * theta + 2. * q1x * qperpz * s000 * theta +
                    2. * q1x * qperpw * s010 * theta + 2. * q1w * qperpx * s010 * theta +
                    2. * q1z * qperpy * s010 * theta + 2. * q1y * qperpz * s010 * theta -
                    4. * q1x * qperpx * s020 * theta - 4. * q1y * qperpy * s020 * theta),
                (q1w * q1y * s001 - q1x * q1z * s001 - qperpw * qperpy * s001 +
                    qperpx * qperpz * s001 - q1w * q1x * s011 - q1y * q1z * s011 +
                    qperpw * qperpx * s011 + qperpy * qperpz * s011 +
                    q1x * q1x * s021 + q1y * q1y * s021 - qperpx * qperpx * s021 -
                    qperpy * qperpy * s021 - q1w * q1y * s101 + q1x * q1z * s101 +
                    qperpw * qperpy * s101 - qperpx * qperpz * s101 +
                    q1w * q1x * s111 + q1y * q1z * s111 - qperpw * qperpx * s111 -
                    qperpy * qperpz * s111 - q1x * q1x * s121 - q1y * q1y * s121 +
                    qperpx * qperpx * s121 + qperpy * qperpy * s121 -
                    2. * q1y * qperpw * s001 * theta + 2. * q1z * qperpx * s001 * theta -
                    2. * q1w * qperpy * s001 * theta + 2. * q1x * qperpz * s001 * theta +
                    2. * q1x * qperpw * s011 * theta + 2. * q1w * qperpx * s011 * theta +
                    2. * q1z * qperpy * s011 * theta + 2. * q1y * qperpz * s011 * theta -
                    4. * q1x * qperpx * s021 * theta - 4. * q1y * qperpy * s021 * theta),
                (q1w * q1y * s002 - q1x * q1z * s002 - qperpw * qperpy * s002 +
                    qperpx * qperpz * s002 - q1w * q1x * s012 - q1y * q1z * s012 +
                    qperpw * qperpx * s012 + qperpy * qperpz * s012 +
                    q1x * q1x * s022 + q1y * q1y * s022 - qperpx * qperpx * s022 -
                    qperpy * qperpy * s022 - q1w * q1y * s102 + q1x * q1z * s102 +
                    qperpw * qperpy * s102 - qperpx * qperpz * s102 +
                    q1w * q1x * s112 + q1y * q1z * s112 - qperpw * qperpx * s112 -
                    qperpy * qperpz * s112 - q1x * q1x * s122 - q1y * q1y * s122 +
                    qperpx * qperpx * s122 + qperpy * qperpy * s122 -
                    2. * q1y * qperpw * s002 * theta + 2. * q1z * qperpx * s002 * theta -
                    2. * q1w * qperpy * s002 * theta + 2. * q1x * qperpz * s002 * theta +
                    2. * q1x * qperpw * s012 * theta + 2. * q1w * qperpx * s012 * theta +
                    2. * q1z * qperpy * s012 * theta + 2. * q1y * qperpz * s012 * theta -
                    4. * q1x * qperpx * s022 * theta -
                    4. * q1y * qperpy * s022 * theta));

            let c32 = DerivativeTerm::new(
                0., -2. * (-(q1w * qperpy * s000) + q1x * qperpz * s000 +
                    q1x * qperpw * s010 + q1w * qperpx * s010 -
                    2. * q1x * qperpx * s020 + q1w * qperpy * s100 -
                    q1x * qperpz * s100 - q1x * qperpw * s110 -
                    q1w * qperpx * s110 +
                    q1z * (qperpx * s000 + qperpy * s010 - qperpx * s100 -
                        qperpy * s110) +
                    2. * q1x * qperpx * s120 +
                    q1y * (qperpz * s010 - 2. * qperpy * s020 +
                        qperpw * (-s000 + s100) - qperpz * s110 +
                        2. * qperpy * s120)) *
                    theta,
                -2. * (-(q1w * qperpy * s001) + q1x * qperpz * s001 +
                    q1x * qperpw * s011 + q1w * qperpx * s011 -
                    2. * q1x * qperpx * s021 + q1w * qperpy * s101 -
                    q1x * qperpz * s101 - q1x * qperpw * s111 -
                    q1w * qperpx * s111 +
                    q1z * (qperpx * s001 + qperpy * s011 - qperpx * s101 -
                        qperpy * s111) +
                    2. * q1x * qperpx * s121 +
                    q1y * (qperpz * s011 - 2. * qperpy * s021 +
                        qperpw * (-s001 + s101) - qperpz * s111 +
                        2. * qperpy * s121)) *
                    theta,
                -2. * (-(q1w * qperpy * s002) + q1x * qperpz * s002 +
                    q1x * qperpw * s012 + q1w * qperpx * s012 -
                    2. * q1x * qperpx * s022 + q1w * qperpy * s102 -
                    q1x * qperpz * s102 - q1x * qperpw * s112 -
                    q1w * qperpx * s112 +
                    q1z * (qperpx * s002 + qperpy * s012 - qperpx * s102 -
                        qperpy * s112) +
                    2. * q1x * qperpx * s122 +
                    q1y * (qperpz * s012 - 2. * qperpy * s022 +
                        qperpw * (-s002 + s102) - qperpz * s112 +
                        2. * qperpy * s122)) *
                    theta);

            let c42 = DerivativeTerm::new(
                0.,
                q1w * qperpy * s000 - q1x * qperpz * s000 - q1x * qperpw * s010 -
                    q1w * qperpx * s010 + 2. * q1x * qperpx * s020 -
                    q1w * qperpy * s100 + q1x * qperpz * s100 +
                    q1x * qperpw * s110 + q1w * qperpx * s110 -
                    2. * q1x * qperpx * s120 - 2. * qperpw * qperpy * s000 * theta +
                    2. * qperpx * qperpz * s000 * theta -
                    2. * q1w * q1x * s010 * theta +
                    2. * qperpw * qperpx * s010 * theta +
                    2. * qperpy * qperpz * s010 * theta +
                    2. * q1x * q1x * s020 * theta + 2. * q1y * q1y * s020 * theta -
                    2. * qperpx * qperpx * s020 * theta -
                    2. * qperpy * qperpy * s020 * theta +
                    q1z * (-(qperpx * s000) - qperpy * s010 + qperpx * s100 +
                        qperpy * s110 - 2. * q1x * s000 * theta) +
                    q1y * (-(qperpz * s010) + 2. * qperpy * s020 +
                        qperpw * (s000 - s100) + qperpz * s110 -
                        2. * qperpy * s120 + 2. * q1w * s000 * theta -
                        2. * q1z * s010 * theta),
                q1w * qperpy * s001 - q1x * qperpz * s001 - q1x * qperpw * s011 -
                    q1w * qperpx * s011 + 2. * q1x * qperpx * s021 -
                    q1w * qperpy * s101 + q1x * qperpz * s101 +
                    q1x * qperpw * s111 + q1w * qperpx * s111 -
                    2. * q1x * qperpx * s121 - 2. * qperpw * qperpy * s001 * theta +
                    2. * qperpx * qperpz * s001 * theta -
                    2. * q1w * q1x * s011 * theta +
                    2. * qperpw * qperpx * s011 * theta +
                    2. * qperpy * qperpz * s011 * theta +
                    2. * q1x * q1x * s021 * theta + 2. * q1y * q1y * s021 * theta -
                    2. * qperpx * qperpx * s021 * theta -
                    2. * qperpy * qperpy * s021 * theta +
                    q1z * (-(qperpx * s001) - qperpy * s011 + qperpx * s101 +
                        qperpy * s111 - 2. * q1x * s001 * theta) +
                    q1y * (-(qperpz * s011) + 2. * qperpy * s021 +
                        qperpw * (s001 - s101) + qperpz * s111 -
                        2. * qperpy * s121 + 2. * q1w * s001 * theta -
                        2. * q1z * s011 * theta),
                q1w * qperpy * s002 - q1x * qperpz * s002 - q1x * qperpw * s012 -
                    q1w * qperpx * s012 + 2. * q1x * qperpx * s022 -
                    q1w * qperpy * s102 + q1x * qperpz * s102 +
                    q1x * qperpw * s112 + q1w * qperpx * s112 -
                    2. * q1x * qperpx * s122 - 2. * qperpw * qperpy * s002 * theta +
                    2. * qperpx * qperpz * s002 * theta -
                    2. * q1w * q1x * s012 * theta +
                    2. * qperpw * qperpx * s012 * theta +
                    2. * qperpy * qperpz * s012 * theta +
                    2. * q1x * q1x * s022 * theta + 2. * q1y * q1y * s022 * theta -
                    2. * qperpx * qperpx * s022 * theta -
                    2. * qperpy * qperpy * s022 * theta +
                    q1z * (-(qperpx * s002) - qperpy * s012 + qperpx * s102 +
                        qperpy * s112 - 2. * q1x * s002 * theta) +
                    q1y * (-(qperpz * s012) + 2. * qperpy * s022 +
                        qperpw * (s002 - s102) + qperpz * s112 -
                        2. * qperpy * s122 + 2. * q1w * s002 * theta -
                        2. * q1z * s012 * theta));

            let c52 = DerivativeTerm::new(
                0., 2. * (qperpw * qperpy * s000 - qperpx * qperpz * s000 +
                    q1y * q1z * s010 - qperpw * qperpx * s010 -
                    qperpy * qperpz * s010 - q1y * q1y * s020 +
                    qperpx * qperpx * s020 + qperpy * qperpy * s020 +
                    q1x * q1z * (s000 - s100) - qperpw * qperpy * s100 +
                    qperpx * qperpz * s100 +
                    q1w * (q1y * (-s000 + s100) + q1x * (s010 - s110)) -
                    q1y * q1z * s110 + qperpw * qperpx * s110 +
                    qperpy * qperpz * s110 + q1y * q1y * s120 -
                    qperpx * qperpx * s120 - qperpy * qperpy * s120 +
                    q1x * q1x * (-s020 + s120)) *
                    theta,
                2. * (qperpw * qperpy * s001 - qperpx * qperpz * s001 +
                       q1y * q1z * s011 - qperpw * qperpx * s011 -
                    qperpy * qperpz * s011 - q1y * q1y * s021 +
                    qperpx * qperpx * s021 + qperpy * qperpy * s021 +
                    q1x * q1z * (s001 - s101) - qperpw * qperpy * s101 +
                    qperpx * qperpz * s101 +
                    q1w * (q1y * (-s001 + s101) + q1x * (s011 - s111)) -
                    q1y * q1z * s111 + qperpw * qperpx * s111 +
                    qperpy * qperpz * s111 + q1y * q1y * s121 -
                    qperpx * qperpx * s121 - qperpy * qperpy * s121 +
                    q1x * q1x * (-s021 + s121)) *
                    theta,
                2. * (qperpw * qperpy * s002 - qperpx * qperpz * s002 +
                    q1y * q1z * s012 - qperpw * qperpx * s012 -
                    qperpy * qperpz * s012 - q1y * q1y * s022 +
                    qperpx * qperpx * s022 + qperpy * qperpy * s022 +
                    q1x * q1z * (s002 - s102) - qperpw * qperpy * s102 +
                    qperpx * qperpz * s102 +
                    q1w * (q1y * (-s002 + s102) + q1x * (s012 - s112)) -
                    q1y * q1z * s112 + qperpw * qperpx * s112 +
                    qperpy * qperpz * s112 + q1y * q1y * s122 -
                    qperpx * qperpx * s122 - qperpy * qperpy * s122 +
                    q1x * q1x * (-s022 + s122)) *
                    theta);

            AnimatedTransform {
               start_transform: start.clone(),
                start_time,
                end_transform: end.clone(),
                end_time,
                actually_animated,
                r: [r1, r2],
                s: [s1, s2],
                t: [t1, t2],
                rotation: Rotation::HasRotation(
                    [
                        [c10, c11, c12],
                        [c20, c21, c22],
                        [c30, c31, c32],
                        [c40, c41, c42],
                        [c50, c51, c52]
                    ]
                )
            }
        } else {
            AnimatedTransform {
                start_transform: start.clone(),
                start_time,
                end_transform: end.clone(),
                end_time,
                actually_animated,
                r: [r1, r2],
                s: [s1, s2],
                t: [t1, t2],
               rotation: Rotation::NoRotation
            }
        }


    }
    pub fn has_rotation(&self) -> bool {
        match self.rotation {
            Rotation::NoRotation => false,
            _ => true
        }
    }

    fn interpolate(&self, time: f64) -> Rc<Transform> {
        if !self.actually_animated || time <= self.start_time {
            return self.start_transform.clone();
        }
        if time >= self.end_time {
            return self.end_transform.clone();
        }
        let dt = (time - self.start_time) / (self.end_time - self.start_time);

        let trans = self.t[0] * (1. - dt) + self.t[1] * dt;
        let rotate = slerp(dt, self.r[0], self.r[1]);
        let mut scale = Matrix4f::identity();
        use math::lerp;
        for i in 0..3 {
            for j in 0..3 {
                scale[i][j] = lerp(dt, self.s[0][i][j], self.s[1][i][j]);
            }
        }
        Rc::new(Transform::new_translate(&trans) * rotate.into() * Transform::new(scale))
    }

    pub fn apply<T: Animatable>(&self, target: &T) -> <T as Animatable>::Output {
        target.apply_animation(self)
    }

    pub fn motion_bounds(&self, b: &Bounds3f) -> Bounds3f {
        use bounds::{bounds_union3};
        if !self.actually_animated {
            return self.start_transform.apply(b);
        }

        match self.rotation {
            Rotation::NoRotation => {
                bounds_union3(&self.start_transform.apply(b),
                              &self.end_transform.apply(b))
            },
            Rotation::HasRotation(ref terms) => {
                use std::iter::IntoIterator;
                b.clone()
                    .corners()
                    .into_iter()
                    .fold(Bounds3f::default(), |acc, x|
                        bounds_union3(&acc, &self.bound_point_motion(&x, terms))
                    )
            }
        }


    }

    fn bound_point_motion(&self, p: &Point3f, derivative_terms: &[[DerivativeTerm; 3]; 5]) -> Bounds3f {
        use utils::{clamp, interval_find_zeroes, Interval};
        use math::lerp;
        use bounds::bound_point_union3;
        let mut bounds = Bounds3f::new(self.start_transform.apply(p),
                                       self.end_transform.apply(p));
        let cos_theta = dot_quarternion(self.r[0], self.r[1]);
        let theta = clamp(cos_theta, -1., 1.).acos();
        for c in 0..3 {
            let zeroes = interval_find_zeroes(derivative_terms[0][c].eval(p),
                                              derivative_terms[1][c].eval(p),
                                              derivative_terms[2][c].eval(p),
                                              derivative_terms[3][c].eval(p),
                                              derivative_terms[4][c].eval(p),
                                              theta,
                                              Interval::new(0., 1.),
                                              8);
            for i in 0..zeroes.1 {
                let pz = self.apply(&(lerp(zeroes.0[i], self.start_time, self.end_time), *p));
                bounds = bound_point_union3(&bounds, p);
            }
        }
        bounds
    }


}

fn decompose(m: &Matrix4f) -> (Vec3f, Quarternion, Matrix4f) {
    let t = [m[0][3], m[1][3], m[2][3]];
    let mut without_translation = m.clone();

    for i in 0..3 {
        without_translation[i][3] = 0.;
        without_translation[3][i] = 0.
    }
    without_translation[3][3] = 1.;

    use num::Float;
    let mut norm = f64::max_value();
    let mut count = 0;
    let mut r = without_translation.clone();

    while count < 100 && norm > 0.0001 {
        let mut rnext = Matrix4f::identity();
        let rit = inverse(&transposed(&rnext)).expect("Error inversing matrix");

        for i in 0..4 {
            for j in 0..4 {
                rnext[i][j] = 0.5 * (r[i][j] + rit[i][j]);
            }
        }

        norm = 0.;
        for i in 0..3 {
            let n = (r[i][0] - rnext[i][0]).abs() +
                (r[i][1] - rnext[i][1]).abs() +
                (r[i][2] - rnext[i][2]).abs();
            norm = if norm > n { norm } else { n };
        }

        count += 1;
        r = rnext;
    }

    let s = inverse(&r).expect("Error inversing matrix") * m;

    (t.into(), Quarternion::from_transform(&r.into()), s)
}

