use std::cell::RefCell;
use std::ops::Deref;

// TODO: Use RC/ARC to be able to handle composition
pub struct Lazy<O> {
    v: RefCell<Option<O>>,
    eval: Box<Fn() -> O>
}

impl <O> Lazy<O>  {
   pub fn new<F: Fn() -> O + 'static>(eval: F) -> Self {
       Lazy {
           v: RefCell::new(None),
           eval: Box::new(eval)
       }
   }

//    pub fn compose<T, F: FnOnce(O) -> T + 'static>(self, compose_eval: F) -> Lazy<T> {
//        Lazy {
//            v: RefCell::new(None),
//            eval: Box::new(move || compose_eval(*self))
//        }
//    }
}

impl <O>Deref for Lazy<O> {
   type Target = O;

    fn deref(&self) -> &Self::Target {
        match *self.v.borrow() {
            Some(_) => {
                unsafe {
                    (*self.v.as_ptr()).as_ref().unwrap()
                }
            }
            None => {
                self.v.replace(Some((self.eval)()));
                self.deref()
            }
        }
    }
}