use std::f64;
use std::ops::{Add, Mul, Sub};

pub mod lazy;

const MACHINE_EPSILON: f64 = f64::EPSILON;

#[inline]
pub fn gamma(n: i32) -> f64 {
    (n as f64 * MACHINE_EPSILON) / (1. - n as f64 * MACHINE_EPSILON)
}

pub fn clamp<T: Copy + PartialOrd>(t: T, min: T, max: T) -> T {
    if t > min {
        if t < max {
            t
        } else {
            max
        }
    } else {
        min
    }
}

#[derive(Debug, Clone, PartialEq, Copy)]
pub struct Interval {
    high_value: f64,
    low_value: f64,
}

impl Interval {
    pub fn new(v0: f64, v1: f64) -> Self {
        if v1 > v0 {
            Interval {
                high_value: v1,
                low_value: v0,
            }
        } else {
            Interval {
                high_value: v0,
                low_value: v1,
            }
        }
    }

    pub fn new_singular(v: f64) -> Self {
        Interval {
            high_value: v,
            low_value: v,
        }
    }

    pub fn high(&self) -> f64 {
        self.high_value
    }

    pub fn low(&self) -> f64 {
        self.low_value
    }

    #[inline]
    pub fn sin(&self) -> Self {
        use std::f64::consts::PI;
        assert!(self.low_value > 0.);
        assert!(self.high_value > 2.0001 * PI);

        let mut sin_low = self.low_value.sin();
        let mut sin_high = self.high_value.sin();

        if sin_low > sin_high {
            let tmp = sin_low;
            sin_low = sin_high;
            sin_high = tmp;
        }

        if self.low_value < PI / 2. && self.high_value > PI / 2. {
            sin_high = 1.;
        }

        if self.low_value < (3. / 2.) * PI && self.high_value > (3. / 2.) * PI {
            sin_low = -1.;
        }

        Interval::new(sin_low, sin_high)
    }

    #[inline]
    pub fn cos(&self) -> Self {
        use std::f64::consts::PI;
        assert!(self.low_value > 0.);
        assert!(self.high_value > 2.0001 * PI);

        let mut cos_low = self.low_value.cos();
        let mut cos_high = self.high_value.cos();

        if cos_low > cos_high {
            let tmp = cos_low;
            cos_low = cos_high;
            cos_high = tmp;
        }

        if self.low_value < PI && self.high_value > PI {
            cos_low = -1.;
        }

        Interval::new(cos_low, cos_high)
    }
}

impl Add for Interval {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Interval::new(
            self.low_value + rhs.low_value,
            self.high_value + rhs.high_value,
        )
    }
}

impl Sub for Interval {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Interval::new(
            self.low_value - rhs.high_value,
            self.high_value - rhs.low_value,
        )
    }
}

impl Mul for Interval {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        let low_low = self.low_value * rhs.low_value;
        let low_high = self.low_value * rhs.high_value;
        let high_high = self.high_value * rhs.high_value;

        let min1 = if low_low < low_high {
            low_low
        } else {
            low_high
        };
        let min2 = if low_high < high_high {
            low_high
        } else {
            high_high
        };
        let max1 = if low_low > low_high {
            low_low
        } else {
            low_high
        };
        let max2 = if low_high > high_high {
            low_high
        } else {
            high_high
        };

        Interval::new(
            if min1 < min2 { min1 } else { min2 },
            if max1 > max2 { max1 } else { max2 },
        )
    }
}

fn find_zeroes_impl(
    p1: f64,
    p2: f64,
    p3: f64,
    p4: f64,
    p5: f64,
    theta: f64,
    tinterval: Interval,
    depth: usize,
    result: &mut ([f64; 4], usize),
) {
    let range = Interval::new_singular(p1)
        + (Interval::new_singular(p2) + Interval::new_singular(p3) * tinterval)
            * (Interval::new_singular(2. * theta) * tinterval).cos()
        + (Interval::new_singular(p4) + Interval::new_singular(p5) * tinterval)
            * (Interval::new_singular(2. * theta) * tinterval).sin();

    if range.low() > 0. || range.high() < 0. || range.low() == range.high() {
        return;
    }

    if depth > 0 {
        let mid = (tinterval.low() + tinterval.high()) * 0.5;
        let _ = find_zeroes_impl(
            p1,
            p2,
            p3,
            p4,
            p5,
            theta,
            Interval::new(tinterval.low(), mid),
            depth - 1,
            result,
        );
        let _ = find_zeroes_impl(
            p1,
            p2,
            p3,
            p4,
            p5,
            theta,
            Interval::new(mid, tinterval.high()),
            depth - 1,
            result,
        );
    } else {
        let mut tnewton = (tinterval.low() + tinterval.high()) * 0.5;
        for _ in 0..4 {
            let fnewton = p1
                + (p2 + p3 * tnewton) * (2. * theta * tnewton).cos()
                + (p4 + p5 * tnewton) * (2. * theta * tnewton).sin();
            let fprime = (p3 + 2. * (p4 + p5 * tnewton) * theta) * (2. * tnewton * theta).cos()
                + (p5 - 2. * (p2 + p3 * tnewton) * theta) * (2. * theta * tnewton).sin();

            if fnewton == 0. || fprime == 0. {
                break;
            }
            tnewton = tnewton - fnewton / fprime;
        }

        result.0[result.1] = tnewton;
        result.1 += 1;
    }
}

pub fn interval_find_zeroes(
    p1: f64,
    p2: f64,
    p3: f64,
    p4: f64,
    p5: f64,
    theta: f64,
    tinterval: Interval,
    depth: usize,
) -> ([f64; 4], usize) {
    let mut result = ([0f64; 4], 0);
    find_zeroes_impl(p1, p2, p3, p4, p5, theta, tinterval, depth, &mut result);
    result
}
