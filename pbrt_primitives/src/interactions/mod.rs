use math::normals::Normal3f;
use math::points::{Point2f, Point3f};
use math::vectors::Vector3f;

// TODO: write this code after we have a better idea of how it's to be used

pub trait Interactable {
    fn swap_handness(&self) -> bool;
    fn reverse_orientation(&self) -> bool;
}

pub struct MediumInterface {}

pub struct SurfaceInteraction<'obj, T: Interactable + 'obj + ?Sized> {
    p: Point3f,
    time: f64,
    p_error: Vector3f,
    w0: Vector3f,
    n: Normal3f,
    uv: Point2f,
    dpdu: Vector3f,
    dpdv: Vector3f,
    dndu: Normal3f,
    dndv: Normal3f,
    obj: &'obj T,
    medium: Option<MediumInterface>,
    shadding: Shadding,
}

impl<'a, T: Interactable + 'a> SurfaceInteraction<'a, T> {
    pub fn new<'obj>(
        p: Point3f,
        time: f64,
        p_error: Vector3f,
        uv: Point2f,
        w0: Vector3f,
        dpdu: Vector3f,
        dpdv: Vector3f,
        dndu: Normal3f,
        dndv: Normal3f,
        obj: &'obj T,
    ) -> SurfaceInteraction<'obj, T> {
        let mut n = Normal3f {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        };

        let mut shadding = Shadding {
            n,
            dpdu,
            dpdv,
            dndu,
            dndv,
        };

        if obj.swap_handness() ^ obj.reverse_orientation() {
            n *= -1.;
            shadding.n *= -1.;
        }

        SurfaceInteraction {
            p,
            time,
            p_error,
            w0,
            n,
            uv,
            dpdu,
            dpdv,
            dndu,
            dndv,
            obj,
            medium: None,
            shadding,
        }
    }
}

struct Shadding {
    n: Normal3f,
    dpdu: Vector3f,
    dpdv: Vector3f,
    dndu: Normal3f,
    dndv: Normal3f,
}
