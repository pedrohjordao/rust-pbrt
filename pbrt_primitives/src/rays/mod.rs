use std::cell;

use super::math::points::Point3f;
use super::math::vectors::{abs_vec3, dot3, Vector3f};
use medium::Medium;
use std::rc::Rc;
use transform::{Transform, Transformable, TransformableWithError};

pub struct Ray {
    origin: Point3f,
    direction: Vector3f,
    limit: cell::Cell<f64>,
    time: Option<f64>, // Fazer algo melhor representando tempo
    medium: Option<Rc<Medium>>,
}

#[derive(Debug, Clone)]
pub struct RayTransformError {
    pub origin_error: Vector3f,
    pub direction_error: Vector3f,
}

impl TransformableWithError for Ray {
    type Error = RayTransformError;
    type ErrorComposition = RayTransformError;

    fn apply_transform_get_error(&self, t: &Transform) -> (Self::Output, Self::Error) {
        unimplemented!()
    }

    fn apply_transform_compose_error(
        &self,
        v_error: Self::ErrorComposition,
        t: &Transform,
    ) -> (Self::Output, Self::Error) {
        unimplemented!()
    }
}

impl Transformable for RayDiferential {
    type Output = Self;

    fn apply_transform(&self, t: &Transform) -> Self::Output {
        RayDiferential::new(
            t.apply(&self.base),
            t.apply(&self.rx_origin),
            t.apply(&self.ry_origin),
            t.apply(&self.rx_direction),
            t.apply(&self.ry_direction),
        )
    }
}

#[inline]
fn apply_transform_on_point_get_error(t: &Transform, p: &Point3f) -> (Point3f, Vector3f) {
    let x = p.x;
    let y = p.y;
    let z = p.z;
    let m = t.transformation_matrix();

    // compute transformed coordinates from point_pt_
    let xp = m[0][0] * x + m[0][1] * y + m[0][2] * z + m[0][3];
    let yp = m[1][0] * x + m[1][1] * y + m[1][2] * z + m[1][3];
    let zp = m[2][0] * x + m[2][1] * y + m[2][2] * z + m[2][3];
    let wp = m[3][0] * x + m[3][1] * y + m[3][2] * z + m[3][3];

    // compute absolute error for transformed point
    let x_abs_sum =
        f64::abs(m[0][0] * x) + f64::abs(m[0][1] * y) + f64::abs(m[0][2] * z) + f64::abs(m[0][3]);
    let y_abs_sum =
        f64::abs(m[1][0] * x) + f64::abs(m[1][1] * y) + f64::abs(m[1][2] * z) + f64::abs(m[1][3]);
    let z_abs_sum =
        f64::abs(m[2][0] * x) + f64::abs(m[2][1] * y) + f64::abs(m[2][2] * z) + f64::abs(m[2][3]);
    use utils::gamma;

    let p_error = Vector3f {
        x: x_abs_sum,
        y: y_abs_sum,
        z: z_abs_sum,
    } * gamma(3);
    assert!(wp != 0.);
    let ret_val = if wp == 1. {
        Point3f {
            x: xp,
            y: yp,
            z: zp,
        }
    } else {
        Point3f {
            x: xp / wp,
            y: yp / wp,
            z: zp / wp,
        }
    };
    (ret_val, p_error)
}

#[inline]
fn apply_transform_on_vec_get_error(t: &Transform, v: &Vector3f) -> (Vector3f, Vector3f) {
    let x = v.x;
    let y = v.y;
    let z = v.z;
    let m = t.transformation_matrix();

    // compute transformed coordinates from vec
    let xp = m[0][0] * x + m[0][1] * y + m[0][2] * z;
    let yp = m[1][0] * x + m[1][1] * y + m[1][2] * z;
    let zp = m[2][0] * x + m[2][1] * y + m[2][2] * z;

    // compute absolute error for transformed vec
    let x_abs_sum = f64::abs(m[0][0] * x) + f64::abs(m[0][1] * y) + f64::abs(m[0][2] * z);
    let y_abs_sum = f64::abs(m[1][0] * x) + f64::abs(m[1][1] * y) + f64::abs(m[1][2] * z);
    let z_abs_sum = f64::abs(m[2][0] * x) + f64::abs(m[2][1] * y) + f64::abs(m[2][2] * z);
    use utils::gamma;

    let p_error = Vector3f {
        x: x_abs_sum,
        y: y_abs_sum,
        z: z_abs_sum,
    } * gamma(3);
    let ret_val = Vector3f {
        x: xp,
        y: yp,
        z: zp,
    };
    (ret_val, p_error)
}

impl Transformable for Ray {
    type Output = Self;

    fn apply_transform(&self, t: &Transform) -> Self::Output {
        let (mut o, o_error) = apply_transform_on_point_get_error(t, &self.origin);
        let d = t.apply(&self.direction);
        let length_squared = d.length_squared();
        let mut t_max = self.limit.clone().take();
        if length_squared > 0. {
            let dt = dot3(abs_vec3(d), o_error) / length_squared;
            o += d * dt;
            t_max -= dt;
        }

        Self {
            origin: o,
            direction: d,
            limit: cell::Cell::new(t_max),
            time: self.time.clone(),
            medium: self.medium.clone(),
        }
    }
}

impl Ray {
    pub fn new(origin: Point3f, direction: Vector3f) -> Self {
        Ray {
            origin,
            direction,
            limit: cell::Cell::new(0.0),
            time: None,
            medium: None,
        }
    }

    pub fn limited_ray(origin: Point3f, direction: Vector3f, limit: f64) -> Self {
        assert!(limit > 0.0);
        Ray {
            origin,
            direction,
            limit: cell::Cell::new(limit),
            time: None,
            medium: None,
        }
    }

    pub fn point_from_ray(&self, t: f64) -> Point3f {
        self.origin + self.direction * t
    }

    pub fn time(&self) -> Option<&f64> {
        self.time.as_ref()
    }

    pub fn limit(&self) -> f64 {
        self.limit.get()
    }

    pub fn direction(&self) -> Vector3f {
        self.direction
    }

    pub fn origin(&self) -> Point3f {
        self.origin
    }
}

pub struct RayDiferential {
    base: Ray,
    has_diferentials: bool,
    rx_origin: Point3f,
    ry_origin: Point3f,
    rx_direction: Vector3f,
    ry_direction: Vector3f,
}

impl RayDiferential {
    pub fn new(
        ray: Ray,
        rx_origin: Point3f,
        ry_origin: Point3f,
        rx_direction: Vector3f,
        ry_direction: Vector3f,
    ) -> Self {
        RayDiferential {
            base: ray,
            has_diferentials: false,
            rx_origin,
            ry_origin,
            rx_direction,
            ry_direction,
        }
    }

    pub fn get_base_ray(&self) -> &Ray {
        &self.base
    }

    pub fn get_base_ray_mut(&mut self) -> &mut Ray {
        &mut self.base
    }

    pub fn scale_diferentials(&mut self, s: f64) {
        self.rx_direction = self.base.direction + (self.rx_direction - self.base.direction) * s;
        self.ry_direction = self.base.direction + (self.ry_direction - self.base.direction) * s;
        self.ry_origin = self.base.origin + (self.ry_origin - self.base.origin) * s;
        self.rx_origin = self.base.origin + (self.rx_origin - self.base.origin) * s;
    }

    pub fn time(&self) -> Option<&f64> {
        self.base.time.as_ref()
    }
}
