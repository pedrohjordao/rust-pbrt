use std::convert::From;
use std::env::Args;

pub struct ProgramOptions {
    pub filename: Option<String>,
}

impl From<Args> for ProgramOptions {
    fn from(a: Args) -> Self {
        ProgramOptions { filename: None }
    }
}
