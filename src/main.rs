extern crate pbrt_data_format;
extern crate pbrt_primitives;

mod utils;

use std::convert::From;
use std::env::args;
use std::fs::File;
use std::io;

use pbrt_data_format::parse_file;
use pbrt_primitives::prelude::*;
use utils::options::*;

fn main() {
    let options = ProgramOptions::from(args());
    let scene = match options.filename {
        Some(ref s) => {
            let file = File::open(s).unwrap();
            let reader = io::BufReader::new(file);
            parse_file(reader)
        }
        None => parse_file(io::stdin()),
    };
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {}
}
