#[macro_use]
extern crate nom;
extern crate pbrt_primitives;

mod parser;

use std::io;
use pbrt_primitives::scene::Scene;


pub fn parse_file<R: io::Read>(reader: R) -> io::Result<Scene> {
    unimplemented!()
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
    }
}
