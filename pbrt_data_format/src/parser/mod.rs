use nom::double;
use nom::space;
use nom::print;
use nom::newline;
use nom::digit;
use nom::IResult;
use nom::types::CompleteByteSlice;
use nom::InputTakeAtPosition;
use nom::AsChar;
use nom::recognize_float;

use pbrt_primitives::scene::Scene;
use pbrt_primitives::math::points::Point3f;
use pbrt_primitives::math::vectors::Vector3f;
use pbrt_primitives::scene::LookAt;

named!(parse_point<&[u8], Point3f>,
     do_parse!(
        many0!(space) >>
        x: double >>
        space >>
        y: double >>
        space >>
        z: double >>

        (Point3f {x, y, z})
     )
);

named!(parse_vec<&[u8], Vector3f>,
     do_parse!(
        many0!(space)  >>
        x: double >>
        space >>
        y: double >>
        space >>
        z: double >>

        (Vector3f {x, y, z})
     )
);

named!(parse_look_at<&[u8], LookAt>,
    do_parse!(
        tag!("LookAt") >>
        space >>
        eye: ws!(parse_point) >>
        lookat: ws!(parse_point) >>
        up_vec: ws!(parse_vec) >>

        (LookAt::new(eye, lookat, up_vec))
    )
);


#[derive(Debug,PartialEq)]
pub struct Color {
    pub red:     u8,
    pub green:   u8,
    pub blue:    u8,
}

use std;

fn from_hex(input: &str) -> Result<u8, std::num::ParseIntError> {
    u8::from_str_radix(input, 16)
}

fn is_hex_digit(c: char) -> bool {
    let c = c as u8;
    (c >= 0x30 && c <= 0x39) || (c >= 0x41 && c <= 0x46) || (c >= 0x61 && c <= 0x66)
}

named!(hex_primary<&str, u8>,
  map_res!(take_while_m_n!(2, 2, is_hex_digit), from_hex)
);

named!(hex_color<&str, Color>,
  do_parse!(
           tag!("#")   >>
    red:   hex_primary >>
    green: hex_primary >>
    blue:  hex_primary >>
    (Color { red, green, blue })
  )
);

#[cfg(test)]
mod test_parsers {
    use super::*;
    #[test]
    fn parse_point_test() {
        use nom::types::CompleteStr;
        let parsed_point = parse_point(CompleteStr("0.25 0.1 0.0\0").as_bytes());
        assert!(parsed_point.is_ok());
        let parsed_point = parsed_point.unwrap();
        // Consumed the whole input
        assert_eq!(parsed_point.0.len(), 1);
        // Point is correct
        assert_eq!(parsed_point.1, Point3f {x: 0.25, y: 0.1, z: 0.0});
    }

    #[test]
    fn parse_color() {
            assert_eq!(hex_color("#2F14DF"), Ok(("", Color {
    red: 47,
    green: 20,
    blue: 223,
  })));
    }

    #[test]
    fn parse_vec_test() {
        let parsed_vec = parse_vec(b"0.25 0.1 0.0\0");
        assert!(parsed_vec.is_ok());
        let parsed_vec = parsed_vec.unwrap();
        // Consumed the whole input
        assert_eq!(parsed_vec.0.len(), 1);
        // Vector is correct
        assert_eq!(parsed_vec.1, Vector3f {x: 0.25, y: 0.1, z: 0.0});
    }

    #[test]
    fn parse_look_at_test() {
        let parsed_look = parse_look_at(b"LookAt 1 4 1.5 \n      .5 .5 0 \n    0 0 1\0");
        assert!(parsed_look.is_ok());
    }

    #[test]
    fn parse_loot_at_test_without_linebreak() {
        let parsed_look = parse_look_at(b"LookAt 1 4 1.5 .5 .5 0 0 0 1\0");
        assert!(parsed_look.is_ok());
    }
}